CREATE TABLE zgrada (
	idZgrade INT NOT NULL,
	nazivZgrade VARCHAR(50) NOT NULL,
	ulicaBroj VARCHAR(255),
	brojPoste VARCHAR(5),
	Mjesto VARCHAR(50),
	upraviteljZgrade VARCHAR(255),
	predstavnikSuvlasnika VARCHAR(255),
	pricuvaPoM2 FLOAT,
	povrsinaZgradeM2 FLOAT,
	napomena TEXT,
	UNIQUE (idZgrade),
	UNIQUE (nazivZgrade),
    	PRIMARY KEY (idZgrade)
);

CREATE TABLE prostor (
	idProstor INT NOT NULL,
	idZgrade INT NOT NULL,
	idVrstaProstora VARCHAR(25) NOT NULL,
	brUlaza VARCHAR(15),
	kat VARCHAR(15),
	brStana VARCHAR(15),
	povrsinaM2 FLOAT,
	sifObjektaUpravitelj VARCHAR(8),
	sifObjektaViO VARCHAR(8),
	sifObjektaToplana VARCHAR(11),
	vlasnik VARCHAR(255),
	vlasnikTel VARCHAR(50),
	vlasnikEmail VARCHAR(50),
	racunImeViO VARCHAR(50),
	racunImeToplana VARCHAR(50),
	napomena TEXT,
	pswVlasnik VARCHAR(50),
	UNIQUE (idProstor),
    PRIMARY KEY (idProstor)
	FOREIGN KEY (idZgrade) REFERENCES zgrada (idZgrade) ON DELETE RESTRICT ON UPDATE RESTRICT,
	FOREIGN KEY (idVrstaProstora) REFERENCES vrstaProstora (idVrstaProstora) ON DELETE RESTRICT ON UPDATE RESTRICT
);

CREATE TABLE vrstaProstora (
	idVrstaProstora VARCHAR(25) NOT NULL,
	koefPricuve FLOAT NOT NULL,
	napomena TEXT,
	UNIQUE (idVrstaProstora),
    	PRIMARY KEY (idVrstaProstora)
);

CREATE TABLE brojiloZgrade (
	idBrojilo INT NOT NULL,
	idZgrade INT NOT NULL,
	idVrstaBrojila VARCHAR(25) NOT NULL,
	oznakaBrojila VARCHAR(50),
	brUlaza VARCHAR(15),
	napomena VARCHAR(255),
	UNIQUE (idBrojilo),
    PRIMARY KEY (idBrojilo),
	FOREIGN KEY (idZgrade) REFERENCES zgrada (idZgrade) ON DELETE RESTRICT ON UPDATE RESTRICT,
	FOREIGN KEY (idVrstaBrojila) REFERENCES vrstaBrojila (idVrstaBrojila) ON DELETE RESTRICT ON UPDATE RESTRICT
);

CREATE TABLE stanjeBrojila (
	idBrojilo INT NOT NULL,
	datumOcitanja timestamp NOT NULL,
	stanje FLOAT NOT NULL,
	stvarnoIliProcjena VARCHAR(25) NOT NULL DEFAULT 'Ocitano',
	tkoJeOcitao VARCHAR(25) NOT NULL,
	napomena VARCHAR(255),
    PRIMARY KEY (idBrojilo,datumOcitanja),
	FOREIGN KEY (idBrojilo) REFERENCES brojiloZgrade (idBrojilo) ON DELETE RESTRICT ON UPDATE RESTRICT
);

CREATE TABLE brojClanova (
	idProstor INT NOT NULL,
	godina Smallint NOT NULL,
	mjesec Smallint NOT NULL,
	brojClanova FLOAT,
	pstOdUkupno FLOAT,
	napomena VARCHAR(255),
    PRIMARY KEY (idProstor,godina,mjesec),
	FOREIGN KEY (idProstor) REFERENCES prostor (idProstor) ON DELETE RESTRICT ON UPDATE RESTRICT
);

CREATE TABLE anketa (
	idAnketa INT NOT NULL,
	idZgrade INT NOT NULL,
	nazivAnkete VARCHAR(255) NOT NULL,
	tekstAnkete TEXT,
	dtPocetak timestamp NOT NULL,
	dtKraj timestamp NOT NULL,
	rezultat VARCHAR(255),
	UNIQUE (idAnketa),
    PRIMARY KEY (idAnketa),
	FOREIGN KEY (idZgrade) REFERENCES zgrada (idZgrade) ON DELETE RESTRICT ON UPDATE RESTRICT
);

CREATE TABLE anketaOdgovorProstor (
	idAnketa INT NOT NULL,
	idProstor INT NOT NULL,
	odgovor VARCHAR(25) NOT NULL,
	odgovorTekst TEXT,
    PRIMARY KEY (idAnketa,idProstor,odgovor),
	FOREIGN KEY (idProstor) REFERENCES prostor (idProstor) ON DELETE RESTRICT ON UPDATE RESTRICT,
	FOREIGN KEY (idAnketa,odgovor) REFERENCES anketaOdgovor (idAnketa,odgovor) ON DELETE RESTRICT ON UPDATE RESTRICT
);

CREATE TABLE vrstaBrojila (
	idVrstaBrojila VARCHAR(25) NOT NULL,
	napomena VARCHAR(255),
	UNIQUE (idVrstaBrojila),
   	PRIMARY KEY (idVrstaBrojila)
);

CREATE TABLE anketaOdgovor (
	idAnketa INT NOT NULL,
	odgovor VARCHAR(25) NOT NULL,
    PRIMARY KEY (idAnketa,odgovor),
	FOREIGN KEY (idAnketa) REFERENCES anketa (idAnketa) ON DELETE RESTRICT ON UPDATE RESTRICT
);

CREATE TABLE parametri (
	idZgrade INT NOT NULL,
	unosClanovaDoDanauMjesecu Smallint NOT NULL,
	pswEditZgrada VARCHAR(50) NOT NULL,
   	PRIMARY KEY (idZgrade),
	FOREIGN KEY (idZgrade) REFERENCES zgrada (idZgrade) ON DELETE RESTRICT ON UPDATE RESTRICT
);

CREATE TABLE obavijest (
	idZgrade INT NOT NULL,
	datumVijeme timestamp NOT NULL,
	naslovObavijesti VARCHAR(100) NOT NULL,
	obavijest TEXT,
	UNIQUE (datumVijeme),
    	PRIMARY KEY (idZgrade,datumVijeme)
);
