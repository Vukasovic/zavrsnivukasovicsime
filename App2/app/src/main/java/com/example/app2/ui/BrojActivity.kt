package com.example.app2.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.app.R
import com.example.app2.data.ApiClient
import com.example.app2.data.ApiClient.getClient
import com.example.app2.data.ApiService
import com.example.app2.data.FullDatabase
import com.example.app2.data.tables.BrojClanova
import com.example.app2.data.tables.SyncTable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_broj.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class BrojActivity : AppCompatActivity(), CheckConnection  {
    val db = FullDatabase.getInstance(this)
    lateinit var client : ApiService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_broj)
        client = getClient
        var id = intent.getStringExtra("ID").toInt()
            var lista:List<BrojClanova> =  db!!.brojClanovaDao().getPromjene(id)
            var zadnjaPromjena = lista.sortedBy { b ->
                var str: String = b.mjesec.toString()
                if (b.mjesec < 10){
                    str.padStart(1,'0')
                }
                b.godina.toString() + str
            }.reversed().get(0)
            tv_broj.text = "Trenutno prijavljenih: " + zadnjaPromjena.brojClanova.toString()
        var currentDate: String = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date())
        var month = currentDate.subSequence(3,5).toString().replace("0", "")
        var year = currentDate.subSequence(6,10).toString()
        if (currentDate.subSequence(0,2).toString().compareTo("16") > 0){
            if (month.toInt() > 11){
                year = (year.toInt() + 1).toString()
                month = "1"
            }
            else{
                month = (month.toInt() + 1).toString()
            }
        }
        tv_inforacun.text = "(Primjenjuje se za račune od " + month + ". mjeseca " + year + ". godine)"
        initListeners(month.toInt(), year.toInt(), id)

    }
    fun initListeners(month: Int, year: Int, id: Int){
        btn_spremiPromjenu.setOnClickListener{
            val etPromjena: String = et_brojClanova.text.toString().trim()
            if (etPromjena.isEmpty()){
                et_brojClanova.error = "Unesite novi broj članova"
                et_brojClanova.requestFocus()
            }else{
                val brClan: Float? = etPromjena.toFloat()
                if(isOnline(this)) {
                    client.postBrojClanova(BrojClanova(id, year, month, brClan, null, null))
                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            Toast.makeText(this, "Promjena uspješna", Toast.LENGTH_SHORT).show()
                            db!!.brojClanovaDao()
                                .insert(BrojClanova(id, year, month, brClan, null, null))
                        }, { it ->
                            Toast.makeText(this, "Promjena Nnupješna", Toast.LENGTH_SHORT).show()
                        })
                }
                else{
                    db!!.brojClanovaDao().insert(BrojClanova(id,year,month,brClan,null,null))
                    db!!.syncDao().insert(SyncTable("BrojClanova", id.toString() + "$$$" + year + "$$$" + month, Calendar.getInstance().time))
                }

                this.finish()
            }
        }
    }
}
