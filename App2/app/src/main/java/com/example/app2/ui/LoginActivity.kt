package com.example.app2.ui

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.strictmode.InstanceCountViolation
import android.util.Log
import android.widget.Toast
import com.example.app.R
import com.example.app2.data.ApiClient
import com.example.app2.data.ApiClient.getClient
import com.example.app2.data.ApiService
import com.example.app2.data.FullDatabase
import com.example.app2.data.tables.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.SchedulerSupport.IO
import io.reactivex.functions.Function6
import io.reactivex.functions.Function7
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import okhttp3.Dispatcher
import java.util.*

class LoginActivity : AppCompatActivity(), CheckConnection {
    var db: FullDatabase? = null
    lateinit var client: ApiService
    lateinit var sinkanje: List<SyncTable>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        db = FullDatabase.getInstance(context = this)
        client = getClient
        Log.d("punjenje", "baza istancirana")
        Log.d("punjenje", "gotovo")
//        db!!.syncDao().nukeSync() // ovo je za kada zeznem sa syncom
        sinkanje = db!!.syncDao().getAll().sortedBy { r -> r.ts }
        if (!sinkanje.isEmpty()) {
            sinkanje.forEach{
                if (it.table.equals("Anketa")){
                        client.postAnketa(db!!.anketaDao().getAnketa(it.key.toInt())).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread()).subscribe({
                                db!!.syncDao().del(it, "Anketa")
                            },{})
                    }
                else if (it.table.equals("Prostor")){
                    client.postProstor(db!!.prostorDao().getProstor(it.key.toInt())).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({
                            db!!.syncDao().del(it, "Prostor")
                        },{})
                }
                else if (it.table.equals("Obavijest")){
                    client.postObavijest(db!!.obavijestDao().getObavijest(it.key.split("$$$")[0].toInt(), Date(it.key.split("$$$")[1].toLong()))).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({
                            db!!.syncDao().del(it, "Obavijest")
                        },{})
                }
                else if (it.table.equals("AnketaOdgovorProstor")){
                    client.postOdgovorProstor(db!!.anketaOdgovorProstorDao().getAnketa(it.key.toInt())).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({
                            db!!.syncDao().del(it, "AnketaOdgovorProstor")
                        },{})
                }
                else if (it.table.equals("BrojClanova")){
                    client.postBrojClanova(db!!.brojClanovaDao().getBrojClanova(it.key.split("$$$")[0].toInt(),
                        it.key.split("$$$")[1].toInt(),it.key.split("$$$")[2].toInt())).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({
                            db!!.syncDao().del(it, "BrojClanova")
                        },{})
                }
            }
        }
        initListeners()


    }

    fun initListeners() {
        btn_signin.setOnClickListener {
            val username = login_korisnik.text.toString().trim()
            val password = login_lozinka.text.toString().trim()
            if (username.isEmpty()) {
                login_korisnik.error = "Unesite korisničko ime"
                login_korisnik.requestFocus()
            } else if (password.isEmpty()) {
                login_lozinka.error = "Unesite lozinku"
                login_lozinka.requestFocus()
            } else {
                login(username, password)
            }
        }
    }

    fun punjenje(prostor: Prostor) {
        Observable.zip(client.getZgrada(prostor.idZgrade).toObservable(),
            client.getVrsteProstora().toObservable(),
            client.getClanovi(prostor.idProstor).toObservable(),
            client.getAnkete(prostor.idZgrade).toObservable(),
            client.getOdgovori(prostor.idZgrade).toObservable(),
            client.getObavijesti(prostor.idZgrade).toObservable(),
            client.getProstori(prostor.idZgrade).toObservable(),
            Function7<Zgrada, List<VrstaProstora>, List<BrojClanova>, List<Anketa>, List<AnketaOdgovor>, List<Obavijest>, List<Prostor>, Unit> { t1, t2, t3, t4, t5, t6, t7 ->
                db!!.zgradaDao().insert(t1)
                t2.forEach {
                    db!!.vpDao().insert(it)
                }
                db!!.prostorDao().insert(prostor)
                t3.forEach {
                    db!!.brojClanovaDao().insert(it)
                }
                t4.forEach {
                    db!!.anketaDao().insert(it)
                }
                t5.forEach {
                    db!!.anketaOdgovorDao().insert(it)
                }
                t6.forEach {
                    db!!.obavijestDao().insert(it)
                }
                t7.forEach{
                    db!!.prostorDao().insert(it)
                }
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({

            var intent: Intent = Intent(this, MainActivity::class.java)
            intent.putExtra("VLASNIK", prostor.vlasnik)
            intent.putExtra("ADRESA", db!!.zgradaDao().getZgrada(prostor.idZgrade).ulicaBroj)
            intent.putExtra("BROJ", prostor.brUlaza)
            intent.putExtra("ID", prostor.idProstor.toString())
            intent.putExtra("IDZGRADE", prostor.idZgrade.toString())
            startActivity(intent)
        },
            {
                Log.d("error1", it.toString())
            })

//        client.getZgrada(prostor.idZgrade).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
//            db!!.zgradaDao().insert(it)
////            Toast.makeText(this, it.toString(), Toast.LENGTH_SHORT).show()
//            Log.d("Zgrada", it.toString())
//        },{})
//        client.getVrsteProstora().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
//            it -> it.forEach{
//                db!!.vpDao().insert(it)
//            }
//            db!!.prostorDao().insert(prostor)
//        },{})
//        client.getClanovi(prostor.idProstor).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
//            it -> it.forEach{
//                db!!.brojClanovaDao().insert(it)
//            }
//        },{})
//        client.getAnkete(prostor.idZgrade).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
//            it -> it.forEach{
//                db!!.anketaDao().insert(it)
//        }
//        },{})
//        client.getOdgovori(prostor.idZgrade).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
//            it -> it.forEach{
//            db!!.anketaOdgovorDao().insert(it)
//        }
//        },{})
//        client.getObavijesti(prostor.idZgrade).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
//            it -> it.forEach {
//            db!!.obavijestDao().insert(it)
//        }
//        },{})
            }

            fun login(username: String, password: String) {
                if(isOnline(this)) {
                    client.getProstor(username, password).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({
                            db!!.anketaOdgovorDao().nukeAnketaOdgovor()
                            db!!.anketaOdgovorProstorDao().nukeAnketaOdgovorProstor()
                            db!!.brojClanovaDao().nukeBrojClanova()
                            db!!.obavijestDao().nukeObavijest()
                            db!!.prostorDao().nukeProstor()
                            db!!.anketaDao().nukeAnketa()

                            punjenje(it)

                        }, {
                            Toast.makeText(this, "Neuspješna prijava", Toast.LENGTH_SHORT).show()
                        })
                }
                else{
                    var prostor: Prostor? = db?.prostorDao()?.getProstorByUser(username, password)
                    if (prostor != null){
                        var intent: Intent = Intent(this, MainActivity::class.java)
                        intent.putExtra("VLASNIK", prostor.vlasnik)
                        intent.putExtra("ADRESA", db!!.zgradaDao().getZgrada(prostor.idZgrade).ulicaBroj)
                        intent.putExtra("BROJ", prostor.brUlaza)
                        intent.putExtra("ID", prostor.idProstor.toString())
                        intent.putExtra("IDZGRADE", prostor.idZgrade.toString())
                        startActivity(intent)
                        Toast.makeText(this, "Rad bez interneta", Toast.LENGTH_SHORT).show()
                    }
                    else{
                        Toast.makeText(this, "Neuspješna prijava", Toast.LENGTH_SHORT).show()
                    }
                }
            }
    }
