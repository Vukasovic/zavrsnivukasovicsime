package com.example.app2.data.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import java.util.*

@Entity(
    tableName = "stanjeBrojila",
    primaryKeys = arrayOf("idBrojilo", "datumOcitanja"),
    foreignKeys = arrayOf(
        ForeignKey(
            entity = BrojiloZgrade::class,
            parentColumns = arrayOf("idBrojilo"),
            childColumns = arrayOf("idBrojilo")
        )
    )
)
data class StanjeBrojila(
    val idBrojilo: Int,
    val datumOcitanja: Date,
    val stanje: Float,
    val stvarnoIliProcjena: String,
    val tkoJeOcitao: String,
    val napomena: String?
//PRIMARY KEY (idBrojilo,datumOcitanja)
)