package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import com.google.gson.annotations.SerializedName

@Entity(
    tableName = "brojClanova",
    indices = arrayOf(Index(value = ["godina", "mjesec"])),
    primaryKeys = arrayOf("idProstor", "godina", "mjesec"),
    foreignKeys = arrayOf(
        ForeignKey(entity = Prostor::class,
            parentColumns = arrayOf("idProstor"),
            childColumns = arrayOf("idProstor"))))
data class BrojClanova(
    @SerializedName("idProstor")
    val idProstor: Int,
    @SerializedName("godina")
    val godina: Int,
    @SerializedName("mjesec")
    val mjesec: Int,
    @SerializedName("brojClanova")
    val brojClanova: Float?,
    @SerializedName("pstOdUkupno")
    val pstOdUkupno: Float?,
    @SerializedName("napomena")
    val napomena: String?
)