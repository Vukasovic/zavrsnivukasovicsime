package com.example.app2.data.DAO

import androidx.room.*
import com.example.app2.data.tables.SyncTable

@Dao
interface SyncDAO {
    @Query("SELECT * FROM `synctable`")
    fun getAll():List<SyncTable>

    @Query("DELETE FROM `synctable` WHERE " +
            ":key = `key` AND :table = `table`")
    fun del(key:String, table:String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(st:SyncTable)

    @Query("DELETE FROM `synctable`")
    fun nukeSync()
}