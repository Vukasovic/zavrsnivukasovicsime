package com.example.app2.data.DAO

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.app2.data.tables.Anketa

@Dao
interface AnketaDAO {
    @Query("SELECT * FROM `anketa`")
    fun getAll(): List<Anketa>

    @Query("SELECT * FROM `anketa` " +
            "WHERE :datum <= dtKraj")
    fun getAllActive(datum: Int): List<Anketa>

    @Query("SELECT * FROM `anketa` " +
            "WHERE :id = idAnketa")
    fun getAnketa(id: Int): Anketa

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(a: Anketa)

    @Query("DELETE FROM `anketa`")
    fun nukeAnketa()
}