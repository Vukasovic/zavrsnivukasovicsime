package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "brojiloZgrade",
    primaryKeys = arrayOf("idBrojilo"),
    foreignKeys = arrayOf(
        ForeignKey(
            entity = Zgrada::class,
            parentColumns = arrayOf("idZgrade"),
            childColumns = arrayOf("idZgrade")
        ),
        ForeignKey(
            entity = VrstaBrojila::class,
            parentColumns = arrayOf("idVrstaBrojila"),
            childColumns = arrayOf("idVrstaBrojila")
        )
    )
)
data class BrojiloZgrade(
    val idBrojilo: Int,
    val idZgrade: Int,
    val idVrstaBrojila: String,
    val oznakaBrojila: String?,
    val brUlaza: String?,
    val napomena: String?
)