package com.example.app2.data.DAO

import androidx.room.*
import com.example.app2.data.tables.AnketaOdgovorProstor

@Dao
interface AnketaOdgovorProstorDAO {
    @Query("SELECT * FROM `anketaOdgovorProstor` " +
            "WHERE :id = `idAnketa`")
    fun getAnketa(id: Int): AnketaOdgovorProstor

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(anketaOdgovorProstor: AnketaOdgovorProstor)
    @Delete
    fun delete(anketaOdgovorProstor: AnketaOdgovorProstor)

    @Query("DELETE FROM `anketaOdgovorProstor`")
    fun nukeAnketaOdgovorProstor()
}