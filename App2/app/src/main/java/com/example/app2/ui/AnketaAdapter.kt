package com.example.app2.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.app.R
import com.example.app2.data.tables.Anketa
import java.text.SimpleDateFormat

class AnketaAdapter (private val list: List<Anketa>, context: Context, val click:ItemClickListener):
    RecyclerView.Adapter<AnketaAdapter.AnketaViewHolder>() {
    private var context = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnketaAdapter.AnketaViewHolder {
        val inflater = LayoutInflater.from(context).inflate(R.layout.rv_item_2, parent, false)
        return AnketaAdapter.AnketaViewHolder(inflater)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: AnketaViewHolder, position: Int) {
        val a : Anketa = list[position]
        holder.bind(a)
        holder.aTitleView.setOnClickListener{
            click.onClick(a)
        }
    }


    class AnketaViewHolder (itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var aTitleView: TextView = itemView.findViewById(R.id.tv_naziv_ankete)
        var aDateView1: TextView = itemView.findViewById(R.id.tv_datum_pocetka)
        var aDateView2: TextView = itemView.findViewById(R.id.tv_datum_kraja)

        fun bind(a: Anketa){
            with(a) {
                aTitleView?.text = a.nazivAnkete
                aDateView1?.text = SimpleDateFormat("dd-MM-yyyy").format(a.dtPocetak)
                aDateView2?.text = SimpleDateFormat("dd-MM-yyyy").format(a.dtKraj)
            }
        }

    }

    interface ItemClickListener{
        fun onClick(a: Anketa)
    }
}