package com.example.app2.data.DAO

import androidx.room.*
import com.example.app2.data.tables.BrojClanova

@Dao
interface BrojClanovaDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(brojClanova: BrojClanova)
    @Delete
    fun delete(brojClanova: BrojClanova)

    @Query("SELECT * FROM `brojClanova` " +
            "WHERE :id = `idProstor`")
    fun getPromjene(id: Int): List<BrojClanova>

    @Query("SELECT * FROM `brojClanova` " +
            "WHERE :id = `idProstor` " +
            "AND :godina = `godina` " +
            "AND :mjesec = `mjesec`")
    fun getBrojClanova(id: Int, godina: Int, mjesec: Int): BrojClanova

    @Query("DELETE FROM `brojClanova`")
    fun nukeBrojClanova()
}