package com.example.app2.data.DAO

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.app2.data.tables.Zgrada

@Dao
interface ZgradaDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(z: Zgrada)
    @Query("SELECT * FROM `zgrada` WHERE :id = `idZgrade`")
    fun getZgrada(id: Int): Zgrada
}