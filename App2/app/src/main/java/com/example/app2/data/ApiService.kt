package com.example.app2.data

import com.example.app2.data.tables.*
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiService {
    @GET ("anketa/get/{id}")
    fun getAnkete(@Path ("id") id: Int): Single<List<Anketa>>
    @GET("anketa/odgovori/{id}")
    fun getOdgovori(@Path("id") id: Int): Single<List<AnketaOdgovor>>
    @GET("zgrada/vrste")
    fun getVrsteProstora():Single<List<VrstaProstora>>
    @GET("anketa/prostori/{id}")
    fun getAnketaOdgovorProstori(@Path("id") id: Int): Single<List<AnketaOdgovorProstor>>
    @GET("obavijest/get/{id}")
    fun getObavijesti(@Path("id") id: Int): Single<List<Obavijest>>
    @GET("prostor/get/{id}")
    fun getProstori(@Path ("id")id: Int): Single<List<Prostor>>
    @GET("prostor/clanovi/get/{id}")
    fun getClanovi(@Path("id")id: Int): Single<List<BrojClanova>>
    @GET("prostor/login/{email}/{psw}")
    fun getProstor(@Path("email") email: String, @Path("psw")psw: String): Single<Prostor>
    @GET("zgrada/find/{id}")
    fun getZgrada(@Path("id") id: Int): Single<Zgrada>

    @POST ("prostor/post")
    fun postProstor(@Body p:Prostor): Single<String>
    @POST ("prostor/clanovi/post")
    fun postBrojClanova(@Body bc:BrojClanova): Single<String>
    @POST("anketa/odg")
    fun postOdgovorProstor(@Body aop:AnketaOdgovorProstor): Single<String>
    @POST("anketa/post")
    fun postAnketa(@Body a:Anketa): Single<String>
    @POST("obavijest/post")
    fun postObavijest(@Body ob:Obavijest): Single<String>
    @POST("anketa/postodg")
    fun postAnketaOdgovor(@Body ao:AnketaOdgovor): Single<String>

}