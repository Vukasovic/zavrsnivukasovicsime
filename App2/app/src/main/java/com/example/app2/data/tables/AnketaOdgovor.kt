package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "anketaOdgovor",
    primaryKeys = arrayOf("idAnketa", "odgovor"),
    foreignKeys = arrayOf(
        ForeignKey(entity = Anketa::class,
            parentColumns = arrayOf("idAnketa"),
            childColumns = arrayOf("idAnketa")
        )
    )
)
data class AnketaOdgovor(
    val idAnketa: Int,
    val odgovor: String
)