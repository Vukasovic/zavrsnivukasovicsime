package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "parametri",
    primaryKeys = arrayOf("idZgrade"),
    foreignKeys = arrayOf(
        ForeignKey(
            entity = Zgrada::class,
            parentColumns = arrayOf("idZgrade"),
            childColumns = arrayOf("idZgrade")
        )
    )
)
data class Parametri(
    val idZgrade: Int,
    val unosClanovaDoDanauMjesecu: Int,
    val pswEditZgrada: String
)