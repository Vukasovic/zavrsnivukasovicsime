package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "vrstaBrojila",
    primaryKeys = arrayOf("idVrstaBrojila")
)
data class VrstaBrojila(
    val idVrstaBrojila: String,
    val napomena: String?
)