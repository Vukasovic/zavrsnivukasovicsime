package com.example.app2.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.app.R
import com.example.app2.data.ApiClient
import com.example.app2.data.ApiClient.getClient
import com.example.app2.data.ApiService
import com.example.app2.data.FullDatabase
import com.example.app2.data.tables.Prostor
import com.example.app2.data.tables.SyncTable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_kontakt.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*

class KontaktActivity : AppCompatActivity(), CheckConnection  {
    val db = FullDatabase.getInstance(this)
    lateinit var client : ApiService
    var id:Int? = null
    var prostor: Prostor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kontakt)
        client = getClient
        id = intent.getStringExtra("ID")?.toInt()
        prostor = db?.prostorDao()?.getProstor(id!!)
        if (prostor != null){
            tv_telefon.text = "Telefon koristnika: " + prostor!!.vlasnikTel
            tv_email.text = "Email korisnika: " + prostor!!.vlasnikEmail
        }
        initListeners()
    }

    fun initListeners(){
        btn_spremi.setOnClickListener{
            val noviEmail = et_email.text.toString().trim()
            val noviTel = et_telefon.text.toString().trim()
            if (!noviEmail.isEmpty() and noviEmail.contains(Regex("@([q|w|e|r|t|z|u|i|o|p|a|s|d|f|g|h|j|k|l|y|x|c|v|b|n|m|1|2|3|4|5|6|7|8|9|0]+)([.])" +
                        "([q|w|e|r|t|z|u|i|o|p|a|s|d|f|g|h|j|k|l|y|x|c|v|b|n|m|1|2|3|4|5|6|7|8|9|0]+)"))) {
                //id treba biti iz contexta, takodjer, dodati ako ima neta...
                prostor!!.vlasnikEmail = noviEmail
                if (isOnline(this)){
                    client.postProstor(prostor!!).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                    ).subscribe({

                        db?.prostorDao()?.updateEmail(noviEmail, id!!)
                        tv_email.text = "Email korisnika: " + noviEmail
                    }, {
                        Toast.makeText(this, "Podatak nepromijenjen", Toast.LENGTH_SHORT).show()
                    })
                }
                else{
                    db!!.prostorDao().insert(prostor!!)
                    db!!.syncDao().insert(SyncTable("Prostor", prostor!!.idProstor.toString(), Calendar.getInstance().time))
                }
            }
            if (!noviTel.isEmpty()){
                //id treba biti iz contexta, takodjer, dodati ako ima neta...
                prostor!!.vlasnikTel = noviTel
                if(isOnline(this)) {
                    client.postProstor(prostor!!).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                    ).subscribe({

                        db?.prostorDao()?.updateTel(noviTel, id!!)
                        tv_telefon.text = "Telefon korisnika: " + noviTel

                    }, {

                        Toast.makeText(this, "Podatak nepromijenjen", Toast.LENGTH_SHORT).show()
                    })
                }
                else{
                    db!!.prostorDao().insert(prostor!!)
                    db!!.syncDao().insert(SyncTable("Prostor", prostor!!.idProstor.toString(), Calendar.getInstance().time))
                }
            }
            this.finish()
        }
    }
}
