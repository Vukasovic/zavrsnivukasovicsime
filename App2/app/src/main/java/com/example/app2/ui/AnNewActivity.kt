package com.example.app2.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.app.R
import com.example.app2.data.ApiClient
import com.example.app2.data.ApiClient.getClient
import com.example.app2.data.ApiService
import com.example.app2.data.FullDatabase
import com.example.app2.data.tables.Anketa
import com.example.app2.data.tables.AnketaOdgovor
import com.example.app2.data.tables.SyncTable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_an_new.*
import java.text.SimpleDateFormat
import java.util.*

class AnNewActivity : AppCompatActivity(), CheckConnection {
    val db = FullDatabase.getInstance(this)
    lateinit var client : ApiService
    var datKraj: Date = Date(0)
    val formatter = SimpleDateFormat("dd.MM.yyyy")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_an_new)
        client = getClient
        var idZgrade = intent.getStringExtra("IDZGRADE").toInt()

        btn_spremi_an.setOnClickListener{
            val naslov = et_nova_a.text.toString().trim()
            val tekst = et_nov_t_a.text.toString().trim()
            try {
                datKraj= formatter.parse(et_dt_kraj.text.toString())
            }
            catch(e:Exception){
            }
            if (naslov.isEmpty()){
                et_nova_a.error = "Unesite naslov"
                et_nova_a.requestFocus()
            }
            else if (tekst.isEmpty()){
                et_nov_t_a.error = "Unesite tekst"
                et_nov_t_a.requestFocus()
            }
            else if(datKraj == Date(0)){
                et_dt_kraj.error = "Unesite ispravan format datuma"
                et_dt_kraj.requestFocus()
            }
            else{
                if (isOnline(this)){
                    client.postAnketa(Anketa(1, idZgrade, naslov, tekst, Calendar.getInstance().time, datKraj, null)).subscribeOn(
                        Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe( {
                        db!!.anketaDao().insert(Anketa(it.toInt(),  idZgrade, naslov, tekst, Calendar.getInstance().time, datKraj, null))
                        Toast.makeText(this, "Objavljeno", Toast.LENGTH_SHORT).show()
                    },{
                        Toast.makeText(this, "Nije objavljeno", Toast.LENGTH_SHORT).show()
                    })
                }
                else{
                    var id = db!!.anketaDao().getAll().sortedBy { a -> a.idAnketa }.reversed().get(0).idAnketa+1
                    db.anketaDao().insert(Anketa(id, idZgrade, naslov, tekst, Calendar.getInstance().time, datKraj, null))
                    db.syncDao().insert(SyncTable("Anketa", id.toString(), Calendar.getInstance().time))
                }
                this.finish()
            }
        }
    }
}
