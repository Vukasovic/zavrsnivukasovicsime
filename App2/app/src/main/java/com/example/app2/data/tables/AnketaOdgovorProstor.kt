package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "anketaOdgovorProstor",
    primaryKeys = arrayOf("idAnketa","idProstor"),
    foreignKeys = arrayOf(
        ForeignKey(
            entity = Prostor::class,
            parentColumns = arrayOf("idProstor"),
            childColumns = arrayOf("idProstor")
        ),
        ForeignKey(
            entity = Anketa::class,
            parentColumns = arrayOf("idAnketa"),
            childColumns = arrayOf("idAnketa")
        )
    )
)
data class AnketaOdgovorProstor(
    val idAnketa: Int,
    val idProstor: Int,
    val odgovor: String,
    val odgovorTekst: String?
)