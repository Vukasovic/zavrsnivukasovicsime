package com.example.app2.ui

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.app.R
import kotlinx.android.synthetic.main.activity_pop_up_obavijest.*
import java.text.SimpleDateFormat
import java.util.Date

class PopUpObavijestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pop_up_obavijest)
        tv_dt_pop_o.text = intent.getStringExtra("DATUM")
        tv_naslov_pop_o.text = intent.getStringExtra("NASLOV")
        tv_txt_o.text = intent.getStringExtra("TEKST")
    }
}
