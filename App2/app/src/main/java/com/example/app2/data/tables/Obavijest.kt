package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.ForeignKey
import java.util.*

@Entity(
    tableName = "obavijest",
    primaryKeys = arrayOf("idZgrade", "datumVrijeme"),
    foreignKeys = arrayOf(
        ForeignKey(
            entity = Zgrada::class,
            parentColumns = arrayOf("idZgrade"),
            childColumns = arrayOf("idZgrade")
        )
    )
)
data class Obavijest(
    val idZgrade: Int,
    val datumVrijeme: Date,
    val naslovObavijesti: String,
    val obavijest: String?
)