package com.example.app2.data.DAO

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.app2.data.tables.Prostor

@Dao
interface ProstorDAO {
    @Query("SELECT * FROM `prostor`")
    fun getAll(): List<Prostor>

    @Query("UPDATE prostor " +
            "SET `vlasnikTel` = :tel " +
            "WHERE :id = `idProstor`")
    fun updateTel(tel: String?, id: Int)

    @Query("UPDATE `prostor` " +
            "SET `vlasnikEmail` = :email " +
            "WHERE :id = `idProstor`")
    fun updateEmail(email: String?, id: Int)

    @Query("SELECT * FROM `prostor` " +
            "WHERE :id = `idProstor`")
    fun getProstor(id: Int): Prostor

    @Query("SELECT * FROM `prostor` " +
            "WHERE :email = `vlasnikEmail` " +
            "AND :psw = `pswVlasnik`")
    fun getProstorByUser(email: String, psw: String): Prostor?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(p:Prostor)


    @Query("DELETE FROM `prostor`")
    fun nukeProstor()
}