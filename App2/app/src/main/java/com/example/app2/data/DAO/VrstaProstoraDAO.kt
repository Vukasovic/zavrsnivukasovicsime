package com.example.app2.data.DAO

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.example.app2.data.tables.VrstaProstora

@Dao
interface VrstaProstoraDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert (vp: VrstaProstora)
}