package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "prostor",
    primaryKeys = arrayOf("idProstor"),
    foreignKeys = arrayOf(
        ForeignKey(entity = Zgrada::class,
            parentColumns = arrayOf("idZgrade"),
            childColumns = arrayOf("idZgrade")),
        ForeignKey(entity = VrstaProstora::class,
            parentColumns = arrayOf("idVrstaProstora"),
            childColumns = arrayOf("idVrstaProstora"))))
data class Prostor(
    @SerializedName("idProstor")
    val idProstor: Int,
    @SerializedName("idZgrade")
    val idZgrade: Int,
    @SerializedName("idVrstaProstora")
    val idVrstaProstora: String,
    @SerializedName("brUlaza")
    val brUlaza: String?,
    @SerializedName("kat")
    val kat: String?,
    @SerializedName("brStana")
    val brStana: String?,
    @SerializedName("sifObjektaUpravitelj")
    val sifObjektaUpravitelj: String?,
    @SerializedName("sifObjektaViO")
    val sifObjektaViO: String?,
    @SerializedName("sifObjektaToplana")
    val sifObjektaToplana: String?,
    @SerializedName("vlasnik")
    val vlasnik: String?,
    @SerializedName("vlasnikTel")
    var vlasnikTel: String?,
    @SerializedName("vlasnikEmail")
    var vlasnikEmail: String?,
    @SerializedName("racunImeViO")
    val racunImeViO: String?,
    @SerializedName("racunImeToplana")
    val racunImeToplana: String?,
    @SerializedName("napomena")
    val napomena: String?,
    @SerializedName("povrsinaM2")
    val povrsinaM2: Float?,
    @SerializedName("pswVlasnik")
    val pswVlasnik: String?
)