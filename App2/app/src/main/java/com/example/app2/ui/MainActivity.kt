package com.example.app2.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.app.R
import com.example.app2.data.FullDatabase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val db = FullDatabase.getInstance(this)
    lateinit var idZgrade: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val naslov = tv_info
        val ime = intent.getStringExtra("VLASNIK")
        val adresa = intent.getStringExtra("ADRESA")
        val broj = intent.getStringExtra("BROJ")
        val idProstora = intent.getStringExtra("ID")
        idZgrade = intent.getStringExtra("IDZGRADE")
        if (ime == db!!.zgradaDao().getZgrada(idZgrade.toInt()).predstavnikSuvlasnika){
            btn_nova_anketa.visibility = View.VISIBLE
            btn_nova_obavijest.visibility = View.VISIBLE
            btn_izvoz.visibility = View.VISIBLE
        }
        tv_info.text = ime + "\n" + adresa.dropLastWhile { c -> c != ' ' } + " " + broj
        initListeners(idProstora)
    }
    fun initListeners(idProstora:String){
        btn_obavijest.setOnClickListener{
            var intent = Intent(this, ObavijestiActivity::class.java)
            intent.putExtra("ID", idProstora)
            startActivity(intent)
        }
        btn_ankete.setOnClickListener{
            var intent = Intent(this, AnketeActivity::class.java)
            intent.putExtra("ID", idProstora)
            startActivity(intent)
        }
        btn_brojclanova.setOnClickListener{
            var intent = Intent(this, BrojActivity::class.java)
            intent.putExtra("ID", idProstora)
            startActivity(intent)
        }
        btn_kontakt.setOnClickListener{
            var intent = Intent(this, KontaktActivity::class.java)
            intent.putExtra("ID", idProstora)
            startActivity(intent)
        }
        btn_nova_obavijest.setOnClickListener{
            var intent = Intent(this, ObNewActivity::class.java)
            intent.putExtra("IDZGRADE", idZgrade.toString())
            startActivity(intent)
        }
        btn_nova_anketa.setOnClickListener{
            var intent = Intent(this, AnNewActivity::class.java)
            intent.putExtra("IDZGRADE", idZgrade.toString())
            startActivity(intent)
        }
        btn_izvoz.setOnClickListener{
            var intent = Intent(this, GenerateActivity::class.java)
            intent.putExtra("IDZGRADE", idZgrade.toString())
            startActivity(intent)
        }
    }
}
