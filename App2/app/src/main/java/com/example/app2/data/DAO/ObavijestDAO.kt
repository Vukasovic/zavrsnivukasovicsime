package com.example.app2.data.DAO

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.app2.data.tables.Obavijest
import java.util.*

@Dao
interface ObavijestDAO {
    @Query("SELECT * FROM `obavijest`")
    fun getAll(): List<Obavijest>

    @Query("SELECT * FROM `obavijest` " +
            "WHERE :id = `IdZgrade` " +
            "AND :vrijeme = `datumVrijeme`")
    fun getObavijest(id: Int, vrijeme: Date): Obavijest

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ob: Obavijest)

    @Query("DELETE FROM `obavijest`")
    fun nukeObavijest()
}