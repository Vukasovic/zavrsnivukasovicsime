package com.example.app2.data

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.app2.data.DAO.*
import com.example.app2.data.tables.*
import com.google.gson.internal.bind.DateTypeAdapter
//val MIGRATION_1_2 = object: Migration(1, 2) {
//    override fun migrate(database: SupportSQLiteDatabase) {
//        database.execSQL("ALTER TABLE `anketa` RENAME TO `temp_anketa`;" +
//                "CREATE TABLE `anketa`(\n" +
//                "\t`idAnketa` INTEGER NOT NULL,\n" +
//                "\t`idZgrade` INTEGER NOT NULL,\n" +
//                "\t`nazivAnkete` TEXT NOT NULL,\n" +
//                "\t`tekstAnkete` TEXT,\n" +
//                "\t`dtPocetak` INTEGER NOT NULL,\n" +
//                "\t`dtKraj` INTEGER NOT NULL,\n" +
//                "\t`rezultat` TEXT,\n" +
//                "\tUNIQUE (`idAnketa`),\n" +
//                "PRIMARY KEY (`idAnketa`),\n" +
//                "\tFOREIGN KEY (`idZgrade`) REFERENCES `zgrada` (`idZgrade`)\n" +
//                "); " +
//                "INSERT INTO `anketa` SELECT * FROM `temp_anketa`;" +
//                "DROP TABLE `temp_anketa`;" +
//                "ALTER TABLE `obavijest` RENAME TO `temp_obavijest`;" +
//                "CREATE TABLE `obavijest`(" +
//                "`idZgrade` INTEGER NOT NULL,\n" +
//                "\t`datumVijeme` INTEGER NOT NULL,\n" +
//                "\t`naslovObavijesti` TEXT NOT NULL,\n" +
//                "\t`obavijest` TEXT,\n" +
//                "\tUNIQUE (`datumVijeme`),\n" +
//                "\tPRIMARY KEY (`idZgrade`,datumVijeme)\n" +
//                "); " +
//                "INSERT INTO `obavijest` SELECT * FROM temp_obavijest;" +
//                "DROP TABLE `temp_obavijest`;" +
//                "ALTER TABLE `stanjeBrojila` RENAME TO temp;" +
//                "CREATE TABLE `stanjeBrojila` (\n" +
//                "\t`idBrojilo` INTEGER NOT NULL,\n" +
//                "\t`datumOcitanja` INTEGER NOT NULL,\n" +
//                "\t`stanje` REAL NOT NULL,\n" +
//                "\t`stvarnoIliProcjena` TEXT NOT NULL DEFAULT 'Ocitano',\n" +
//                "\t`tkoJeOcitao` TEXT NOT NULL,\n" +
//                "\t`napomena` TEXT,\n" +
//                "    PRIMARY KEY (`idBrojilo`,`datumOcitanja`),\n" +
//                "\tFOREIGN KEY (`idBrojilo`) REFERENCES `brojiloZgrade` (`idBrojilo`)\n" +
//                "); " +
//                "INSERT INTO `stanjeBrojila` SELECT * FROM `temp`;" +
//                "DROP TABLE `temp`")
//    }
//}

val MIGRATION_1_2 = object: Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE IF NOT EXISTS `synctable` (`table` TEXT,`key` TEXT,`ts` INTEGER, PRIMARY KEY (`table`, `key`))")
    }
}

@Database(
    entities =arrayOf(
        Zgrada::class,
        VrstaBrojila::class,
        VrstaProstora::class,
        Prostor::class,
        BrojClanova::class,
        BrojiloZgrade::class,
        StanjeBrojila::class,
        Obavijest::class,
        Parametri::class,
        Anketa::class,
        AnketaOdgovor::class,
        AnketaOdgovorProstor::class,
        SyncTable::class
    ),
    version = 2
)
@TypeConverters(Converters::class)
abstract class FullDatabase: RoomDatabase() {
    abstract fun syncDao(): SyncDAO
    abstract fun anketaDao(): AnketaDAO
    abstract fun anketaOdgovorDao(): AnketaOdgovorDAO
    abstract fun anketaOdgovorProstorDao(): AnketaOdgovorProstorDAO
    abstract fun brojClanovaDao(): BrojClanovaDAO
    abstract fun obavijestDao(): ObavijestDAO
    abstract fun prostorDao(): ProstorDAO
    abstract fun zgradaDao(): ZgradaDAO
    abstract fun vpDao(): VrstaProstoraDAO

    companion object {

        @Volatile private var INSTANCE: FullDatabase? = null

        @Synchronized
        fun getInstance(context: Context): FullDatabase? {
            if (INSTANCE == null) {
                INSTANCE = buildDatabase(context)
            }
            return INSTANCE
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                FullDatabase::class.java, "baza_podataka.db")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
    }
}
