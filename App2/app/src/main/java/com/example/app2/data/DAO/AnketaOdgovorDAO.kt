package com.example.app2.data.DAO

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.app2.data.tables.AnketaOdgovor

@Dao
interface AnketaOdgovorDAO {
    @Query("SELECT * FROM `anketaOdgovor` " +
            "WHERE :id = `idAnketa`")
    fun getAnketaOdgovor(id: Int): List<AnketaOdgovor>
    @Query("SELECT * FROM `anketaOdgovor` " +
            "WHERE :id = `idAnketa` " +
            "AND :odg = `odgovor`")
    fun getOdgovor(id:Int, odg: String): AnketaOdgovor

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert (ao: AnketaOdgovor)

    @Query("DELETE FROM `anketaOdgovor`")
    fun nukeAnketaOdgovor()
}