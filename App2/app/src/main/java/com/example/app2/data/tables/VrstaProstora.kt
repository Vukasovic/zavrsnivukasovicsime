package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "vrstaProstora",
    primaryKeys = arrayOf("idVrstaProstora")
)
data class VrstaProstora(
    val idVrstaProstora: String,
    val koefPricuve: Float,
    val napomena: String?
)