package com.example.app2.ui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app.R
import com.example.app2.data.ApiClient.getClient
import com.example.app2.data.ApiService
import com.example.app2.data.FullDatabase
import com.example.app2.data.tables.Anketa
import com.example.app2.data.tables.AnketaOdgovorProstor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_generate.*
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.util.*


class GenerateActivity : AppCompatActivity(), AnketaAdapter.ItemClickListener {
    val db = FullDatabase.getInstance(this)
    lateinit var client: ApiService
    val context: Context = this
    lateinit var viewManager: RecyclerView.LayoutManager
    lateinit var lista: ArrayList<Anketa>
    val click : AnketaAdapter.ItemClickListener = this
    lateinit var idProstor:String
    lateinit var listaOdgovora: List<AnketaOdgovorProstor>
    lateinit var odgovori: MutableMap<String, Int>
    lateinit var idZgrade: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generate)
        client = getClient
        idZgrade = intent.getStringExtra("IDZGRADE")
        viewManager = LinearLayoutManager(this)
        var lista1 = db!!.anketaDao().getAll().sortedByDescending { a -> a.dtKraj }
        lista = ArrayList<Anketa>()
        lista1.forEach{
            if (it.dtKraj.time < Calendar.getInstance().time.time){
                lista.add(it)
            }
        }
        val myAdapter : AnketaAdapter = AnketaAdapter(lista, context, click)
        rv_odabir.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = myAdapter
        }
    }

    override fun onClick(a: Anketa) {

        //Traženje dopuštenja za pisanje
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                (context as Activity)!!,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                1
            )
            return
        }
        client.getAnketaOdgovorProstori(idZgrade.toInt()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
            listaOdgovora = it
            odgovori = mutableMapOf("Da" to 0, "Ne" to 0, "Suzdržan" to 0)
            listaOdgovora.forEach{
                odgovori[it.odgovor] = odgovori[it.odgovor]!! + 1
            }
            var imeAnkete = a.nazivAnkete
            var datum = Calendar.getInstance().toString()
            val extStorageDirectory: String = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()
            Log.d("Direktorij", extStorageDirectory)
            //        var folder = File(extStorageDirectory, "Excel")
            //        folder.mkdir()
            var file = File(extStorageDirectory, imeAnkete + ".xlsx") //folder kao prvi argument inace
            try{
                file.createNewFile()
            }
            catch(e: IOException){
                Toast.makeText(this, "Došlo je do greške", Toast.LENGTH_SHORT).show()
            }

            try {
                writeToExcelFile(file, a)
                Toast.makeText(this, "Generiran izvještaj", Toast.LENGTH_SHORT).show()
            } catch (e: Exception) {
                Toast.makeText(this, "Greška", Toast.LENGTH_SHORT).show()
            }

        },{
            Toast.makeText(this, "Greška prvog poziva", Toast.LENGTH_SHORT).show()
        })
    }

    fun writeToExcelFile(filePath: File, a: Anketa){
        var workbook = XSSFWorkbook()
        var createHelper = workbook.getCreationHelper()
        var sheet1 = workbook.createSheet("Generalno")
        var sheet2 = workbook.createSheet("Odgovori")
        var row = sheet1.createRow(0)
        row.createCell(0).setCellValue("Naziv ankete")
        row.createCell(1).setCellValue(a.nazivAnkete)
        row.createCell(2).setCellValue("Broj odgovora:")
        var cell4 = row.createCell(3)
        cell4.setCellValue(listaOdgovora.size.toString())
        var max = 0
        var maxOdg = ""
        odgovori.forEach{
            if (it.value > max){
                max = it.value
                maxOdg = it.key
            }
        }
        row = sheet1.createRow(1)
        row.createCell(0).setCellValue("Najčešći odgovor:")
        row.createCell(1).setCellValue(maxOdg)
        var brojac = 2
        odgovori.forEach{
            row = sheet1.createRow(brojac++)
            row.createCell(0).setCellValue(it.key)
            row.createCell(1).setCellValue(it.value.toString())
        }
//        for (i in 0..brojac-1){
//            sheet1.autoSizeColumn(i)
//        }

        var COLUMNs = arrayOf<String>("Vlasnik", "Odgovor", "Komentar")
        var headerFont = workbook.createFont()
        headerFont.setBold(true)
        headerFont.setColor(IndexedColors.BLACK.getIndex())
        var headerCellStyle = workbook.createCellStyle()
        headerCellStyle.setFont(headerFont)
        var headerRow = sheet2.createRow(0)

        for (col in COLUMNs.indices) {
            var cell = headerRow.createCell(col)
            cell.setCellValue(COLUMNs[col])
            cell.setCellStyle(headerCellStyle)
        }
        brojac = 1
        listaOdgovora.forEach{
            row = sheet2.createRow(brojac++)
            row.createCell(0).setCellValue(db!!.prostorDao().getProstor(it.idProstor).vlasnik)
            row.createCell(1).setCellValue(it.odgovor)
            row.createCell(2).setCellValue(it.odgovorTekst)
        }

//        for (i in 0..2){
//            sheet2.autoSizeColumn(i)
//        }

        //Napiši u file
        var outputStream = FileOutputStream(filePath)
        workbook.write(outputStream)
        outputStream.close()
    }
}
