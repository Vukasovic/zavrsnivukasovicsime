package com.example.app2.ui

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app.R
import com.example.app2.data.FullDatabase
import com.example.app2.data.tables.Obavijest
import kotlinx.android.synthetic.main.activity_obavijesti.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat

class ObavijestiActivity : AppCompatActivity(), ObavijestAdapter.ItemClickListener{
    val db = FullDatabase.getInstance(this)
    val context:Context = this
    lateinit var viewManager: RecyclerView.LayoutManager
    lateinit var lista: List<Obavijest>
    val click : ObavijestAdapter.ItemClickListener = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_obavijesti)
        viewManager = LinearLayoutManager(this)
        runBlocking {
            withContext(Dispatchers.IO) {
                lista = db!!.obavijestDao().getAll().sortedByDescending { o -> o.datumVrijeme }
                val myAdapter:ObavijestAdapter = ObavijestAdapter(lista, context, click)
                rv_obavijesti.apply {
                    setHasFixedSize(true)
                    layoutManager = viewManager
                    adapter = myAdapter
                }
            }
        }
    }

    override fun onClick(ob: Obavijest) {
        var intent = Intent(this, PopUpObavijestActivity::class.java)
        intent.putExtra("NASLOV", ob.naslovObavijesti)
        intent.putExtra("TEKST", ob.obavijest)
        intent.putExtra("DATUM", SimpleDateFormat("dd-MM-yyyy").format(ob.datumVrijeme))
        startActivity(intent)
    }

}
