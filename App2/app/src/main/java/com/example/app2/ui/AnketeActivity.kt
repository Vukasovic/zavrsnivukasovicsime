package com.example.app2.ui

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app.R
import com.example.app2.data.FullDatabase
import com.example.app2.data.tables.Anketa
import kotlinx.android.synthetic.main.activity_ankete.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.ArrayList

class AnketeActivity : AppCompatActivity(), AnketaAdapter.ItemClickListener {
    val db = FullDatabase.getInstance(this)
    val context: Context = this
    lateinit var viewManager: RecyclerView.LayoutManager
    lateinit var lista: ArrayList<Anketa>
    val click : AnketaAdapter.ItemClickListener = this
    lateinit var idProstor:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ankete)
        idProstor = intent.getStringExtra("ID")
        viewManager = LinearLayoutManager(this)
        var lista1 = db!!.anketaDao().getAll().sortedByDescending { a -> a.dtKraj }
        lista = ArrayList<Anketa>()
        lista1.forEach{
            if (it.dtKraj.time > Calendar.getInstance().time.time){
                lista.add(it)
            }
        }
        val myAdapter : AnketaAdapter = AnketaAdapter(lista, context, click)
        rv_ankete.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = myAdapter
        }

    }

    override fun onClick(a: Anketa) {
        var intent = Intent(this, PopUpAnketaActivity::class.java)
        intent.putExtra("IDPROSTOR", idProstor.toString())
        intent.putExtra("IDANKETA", a.idAnketa.toString())
        startActivity(intent)
    }

}
