package com.example.app2.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.app.R
import com.example.app2.data.ApiClient
import com.example.app2.data.ApiClient.getClient
import com.example.app2.data.ApiService
import com.example.app2.data.FullDatabase
import com.example.app2.data.tables.Obavijest
import com.example.app2.data.tables.SyncTable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_ob_new.*
import java.time.LocalDateTime
import java.util.*

class ObNewActivity : AppCompatActivity(), CheckConnection {
    val db = FullDatabase.getInstance(this)
    lateinit var client : ApiService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ob_new)
        client = getClient
        var idZgrade = intent.getStringExtra("IDZGRADE").toInt()

        btn_spremi_ob.setOnClickListener{
            val naslov = et_nova_o.text.toString().trim()
            val tekst = et_nov_t.text.toString().trim()
            if (naslov.isEmpty()){
                et_nova_o.error = "Unesite naslov"
                et_nova_o.requestFocus()
            }
            else if (tekst.isEmpty()){
                et_nov_t.error = "Unesite tekst"
                et_nov_t.requestFocus()
            }
            else{
                if(isOnline(this)){
                    client.postObavijest(Obavijest(idZgrade, Calendar.getInstance().time, naslov,  tekst)).subscribeOn(
                        Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe( {
                        db!!.obavijestDao().insert(Obavijest(idZgrade, Calendar.getInstance().time, naslov,  tekst))
                        Log.d("obavijest", it.toString())
                        Toast.makeText(this, "Objavljeno", Toast.LENGTH_SHORT).show()
                        this.finish()
                    },{
                        Toast.makeText(this, "Nije objavljeno", Toast.LENGTH_SHORT).show()
                    })
                }
                else{
                    db!!.obavijestDao().insert(Obavijest(idZgrade, Calendar.getInstance().time, naslov, tekst))
                    db!!.syncDao().insert(SyncTable("Obavijest", idZgrade.toString() + "$$$" + Calendar.getInstance().time, Calendar.getInstance().time))
                }
            }
        }
    }
}
