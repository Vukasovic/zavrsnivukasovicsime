package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.ForeignKey
import java.util.*

@Entity(
    tableName = "anketa",
    primaryKeys = arrayOf("idAnketa"),
    foreignKeys = arrayOf(
        ForeignKey(entity = Zgrada::class,
            parentColumns = arrayOf("idZgrade"),
            childColumns = arrayOf("idZgrade"))
    )
)
data class Anketa(
    val idAnketa: Int,
    val idZgrade: Int,
    val nazivAnkete: String,
    val tekstAnkete: String?,
    val dtPocetak: Date,
    val dtKraj: Date,
    val rezultat:String?
)