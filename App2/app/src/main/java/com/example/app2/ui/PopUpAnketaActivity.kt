package com.example.app2.ui

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.app.R
import com.example.app2.data.ApiClient
import com.example.app2.data.ApiClient.getClient
import com.example.app2.data.ApiService
import com.example.app2.data.FullDatabase
import com.example.app2.data.FullDatabase.Companion.getInstance
import com.example.app2.data.tables.AnketaOdgovorProstor
import com.example.app2.data.tables.SyncTable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_pop_up_anketa.*
import java.text.SimpleDateFormat
import java.util.*

class PopUpAnketaActivity : AppCompatActivity(), CheckConnection  {
    val db = FullDatabase.getInstance(this)
    var odgKorisnika: String? = null
    lateinit var client : ApiService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pop_up_anketa)
        client = getClient
        val idAnketa = intent.getStringExtra("IDANKETA").toInt()
        val idProstor = intent.getStringExtra("IDPROSTOR").toInt()
        var RgOdgovori = radio_group
        var anketa = db?.anketaDao()?.getAnketa(idAnketa)
        var rb1 = RadioButton(this)
        rb1.setId(View.generateViewId());
        rb1.setText("Da")
        rb1.setOnClickListener{
            var btn = it as RadioButton
            odgKorisnika = btn.getText().toString()
        }
        var rb2 = RadioButton(this)
        rb2.setId(View.generateViewId());
        rb2.setText("Ne")
        rb2.setOnClickListener{
            var btn = it as RadioButton
            odgKorisnika = btn.getText().toString()
        }
        var rb3 = RadioButton(this)
        rb3.setId(View.generateViewId());
        rb3.setText("Suzdržan")
        rb3.setOnClickListener{
            var btn = it as RadioButton
            odgKorisnika = btn.getText().toString()
        }
        RgOdgovori.addView(rb1)
        RgOdgovori.addView(rb2)
        RgOdgovori.addView(rb3)
        var tekstKorisnika:String? = et_multiline.text.toString()
        tv_naslov_pop_a.text = anketa!!.nazivAnkete
        tv_txt_a.text = anketa!!.tekstAnkete
        tv_dt_poc_a.text = SimpleDateFormat("dd-MM-yyyy").format(anketa!!.dtPocetak)
        tv_dt_kraj_a.text = SimpleDateFormat("dd-MM-yyyy").format(anketa!!.dtKraj)

        btn_odgovor.setOnClickListener{
            if (odgKorisnika != null){
                var extraKomentar = et_multiline.text.toString()
                if(isOnline(this)) {
                    client.postOdgovorProstor(
                        AnketaOdgovorProstor(
                            idAnketa,
                            idProstor,
                            odgKorisnika.toString(),
                            extraKomentar
                        )
                    ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
                        db!!.anketaOdgovorProstorDao().insert(
                            AnketaOdgovorProstor(
                                anketa.idAnketa, idProstor,
                                odgKorisnika!!, tekstKorisnika
                            )
                        )
                        Toast.makeText(this, "Odgovoreno", Toast.LENGTH_SHORT).show()
                        finish()
                    }, {
                        Toast.makeText(this, "Neodgovoreno", Toast.LENGTH_SHORT).show()
                    })
                }
                else{
                    db!!.anketaOdgovorProstorDao().insert(AnketaOdgovorProstor(idAnketa, idProstor, odgKorisnika.toString(), extraKomentar))
                    db!!.syncDao().insert(SyncTable("AnketaOdgovorProstor", idAnketa.toString(), Calendar.getInstance().time))
                }
                this.finish()
            }
        }
    }
}
