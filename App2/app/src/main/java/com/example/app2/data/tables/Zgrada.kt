package com.example.app2.data.tables

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "zgrada",
    indices = arrayOf(Index(
        value = ["nazivZgrade"],
        unique = true
    ))
)
data class Zgrada (
    @PrimaryKey
    val idZgrade: Int,
    val nazivZgrade: String,
    val ulicaBroj: String?,
    val brojPoste: String?,
    val mjesto:String?,
    val upraviteljZgrade: String?,
    val predstavnikSuvlasnika : String?,
    val pricuvaPoM2: Float?,
    val povrsinaZgradeM2: Float?,
    val napomena: String?
)