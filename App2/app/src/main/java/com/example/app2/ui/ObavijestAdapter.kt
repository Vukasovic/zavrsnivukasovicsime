package com.example.app2.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.app.R
import com.example.app2.data.tables.Obavijest
import java.text.SimpleDateFormat

class ObavijestAdapter (private val list: List<Obavijest>, context: Context, val click: ItemClickListener):
    RecyclerView.Adapter<ObavijestAdapter.ObavijestViewHolder>() {
    private var context = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObavijestAdapter.ObavijestViewHolder {
        val inflater = LayoutInflater.from(context).inflate(R.layout.rv_item, parent, false)
        return ObavijestAdapter.ObavijestViewHolder(inflater)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ObavijestAdapter.ObavijestViewHolder, position: Int) {
        val ob : Obavijest = list[position]
        holder.bind(ob)
        holder.oTitleView.setOnClickListener{
            click.onClick(ob)
        }
    }


    class ObavijestViewHolder (itemView: View) :
        RecyclerView.ViewHolder(itemView){
        var oTitleView: TextView = itemView.findViewById(R.id.tv_naziv_obavijest)
        var oDateView: TextView? = itemView.findViewById(R.id.tv_datum_obavijest)

//    init {
//        oTitleView = itemView.findViewById(R.id.tv_naziv_obavijest)
//        oDateView = itemView.findViewById(R.id.tv_datum_obavijest)
//    }

        fun bind(ob: Obavijest){
                with(ob){
                    oTitleView?.text = ob.naslovObavijesti
                    oDateView?.text = SimpleDateFormat("dd-MM-yyyy").format(ob.datumVrijeme)
                }
        }

    }
    interface ItemClickListener{
        fun onClick(ob:Obavijest)
    }


}

