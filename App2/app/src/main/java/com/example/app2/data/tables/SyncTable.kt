package com.example.app2.data.tables

import androidx.room.Entity
import java.util.*

@Entity(tableName = "synctable",
        primaryKeys = arrayOf("table","key")
    )
data class SyncTable (
    val table:String,
    val key:String,
    val ts:Date
)