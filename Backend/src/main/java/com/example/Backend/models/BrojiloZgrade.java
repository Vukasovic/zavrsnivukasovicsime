package com.example.Backend.models;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "brojiloZgrade")
public class BrojiloZgrade {

    @NotNull
    @ManyToOne
    private Zgrada zgrada;

    @NotNull
    @ManyToOne
    private VrstaBrojila vrstaBrojila;

    private String oznakaBrojila;
    private String brUlaza;
    private String napomena;
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idBrojilo;

    public BrojiloZgrade(){}

    public BrojiloZgrade(Zgrada zgrada, VrstaBrojila vrstaBrojila, String oznakaBrojila, String brUlaza, String napomena){

        this.zgrada = zgrada;
        this.vrstaBrojila = vrstaBrojila;
        this.oznakaBrojila = oznakaBrojila;
        this.brUlaza = brUlaza;
        this.napomena = napomena;
    }

    public Zgrada getZgrada() {
        return zgrada;
    }

    public VrstaBrojila getVrstaBrojila() {
        return vrstaBrojila;
    }

    public String getOznakaBrojila() {
        return oznakaBrojila;
    }

    public String getBrUlaza() {
        return brUlaza;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public void setZgrada(Zgrada zgrada) {
        this.zgrada = zgrada;
    }

    public void setVrstaBrojila(VrstaBrojila vrstaBrojila) {
        this.vrstaBrojila = vrstaBrojila;
    }

    public void setOznakaBrojila(String oznakaBrojila) {
        this.oznakaBrojila = oznakaBrojila;
    }

    public void setBrUlaza(String brUlaza) {
        this.brUlaza = brUlaza;
    }

    public Long getIdBrojilo() {
        return idBrojilo;
    }

    public void setIdBrojilo(Long idBrojilo) {
        this.idBrojilo = idBrojilo;
    }
}
