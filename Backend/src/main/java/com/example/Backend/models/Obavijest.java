package com.example.Backend.models;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "obavijest"//, uniqueConstraints = {
        //@UniqueConstraint(columnNames = {"zgrada_id_zgrade", "datum_vrijeme"})
//}
)
public class Obavijest {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne
    private Zgrada zgrada;

    private Timestamp datumVrijeme;
    private String naslovObavijesti;
    private String obavijest;

    public  Obavijest(){}

    public Obavijest(Zgrada zgrada, Timestamp datumVrijeme, String naslovObavijesti, String obavijest){

        this.zgrada = zgrada;
        this.datumVrijeme = datumVrijeme;
        this.naslovObavijesti = naslovObavijesti;
        this.obavijest = obavijest;
    }

    public Zgrada getZgrada() {
        return zgrada;
    }

    public void setZgrada(Zgrada zgrada) {
        this.zgrada = zgrada;
    }

    public Timestamp getDatumVrijeme() {
        return datumVrijeme;
    }

    public String getNaslovObavijesti() {
        return naslovObavijesti;
    }

    public void setNaslovObavijesti(String naslovObavijesti) {
        this.naslovObavijesti = naslovObavijesti;
    }

    public String getObavijest() {
        return obavijest;
    }

    public void setObavijest(String obavijest) {
        this.obavijest = obavijest;
    }
}
