package com.example.Backend.models;

import com.sun.istack.NotNull;
import net.bytebuddy.implementation.bind.annotation.Default;
import org.aspectj.lang.annotation.control.CodeGenerationHint;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "stanjeBrojila"//, uniqueConstraints = {
        //@UniqueConstraint(columnNames = {"brojilo_id_brojilo", "datum_ocitanja"})
//}
)
public class StanjeBrojila {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne
    private BrojiloZgrade brojilo;

    private Date datumOcitanja;
    private Float stanje;
    @Column(columnDefinition = "varchar(255) default 'Ocitano'")
    private String stvarnoIliProcjena;
    private String tkoJeOcitao;
    private String napomena;

    public StanjeBrojila(){}

    public StanjeBrojila(BrojiloZgrade brojilo, Date datumOcitanja, Float stanje, String stvarnoIliProcjena, String tkoJeOcitao, String napomena){

        this.brojilo = brojilo;
        this.datumOcitanja = datumOcitanja;
        this.stanje = stanje;
        this.stvarnoIliProcjena = stvarnoIliProcjena;
        this.tkoJeOcitao = tkoJeOcitao;
        this.napomena = napomena;
    }

    public BrojiloZgrade getBrojilo() {
        return brojilo;
    }

    public Date getDatumOcitanja() {
        return datumOcitanja;
    }

    public Float getStanje() {
        return stanje;
    }

    public void setStanje(Float stanje) {
        this.stanje = stanje;
    }

    public String getStvarnoIliProcjena() {
        return stvarnoIliProcjena;
    }

    public void setStvarnoIliProcjena(String stvarnoIliProcjena) {
        this.stvarnoIliProcjena = stvarnoIliProcjena;
    }

    public String getTkoJeOcitao() {
        return tkoJeOcitao;
    }

    public void setTkoJeOcitao(String tkoJeOcitao) {
        this.tkoJeOcitao = tkoJeOcitao;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }
}
