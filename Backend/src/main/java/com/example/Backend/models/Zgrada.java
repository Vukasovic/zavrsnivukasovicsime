package com.example.Backend.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="zgrada")
public class Zgrada {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idZgrade;

    private String nazivZgrade;
    private String ulicaBroj;
    private String brojPoste;
    private String mjesto;
    private String upraviteljZgrade;
    private String predstavnikSuvlasnika;
    private Float pricuvaPoM2;
    private Float povrsinaZgradeM2;
    private String napomena;

    public Zgrada(){}

    public Zgrada(String nazivZgrade, String ulicaBroj, String brojPoste, String mjesto, String upraviteljZgrade, String predstavnikSuvlasnika,
                  Float pricuvaPoM2, Float povrsinaZgradeM2, String napomena){

        this.nazivZgrade = nazivZgrade;
        this.ulicaBroj = ulicaBroj;
        this.brojPoste = brojPoste;
        this.mjesto = mjesto;
        this.upraviteljZgrade = upraviteljZgrade;
        this.predstavnikSuvlasnika = predstavnikSuvlasnika;
        this.pricuvaPoM2 = pricuvaPoM2;
        this.povrsinaZgradeM2 = povrsinaZgradeM2;
        this.napomena = napomena;
    }

    public String getUpraviteljZgrade() {
        return upraviteljZgrade;
    }

    public void setUpraviteljZgrade(String upraviteljZgrade) {
        this.upraviteljZgrade = upraviteljZgrade;
    }

    public String getPredstavnikSuvlasnika() {
        return predstavnikSuvlasnika;
    }

    public void setPredstavnikSuvlasnika(String predstavnikSuvlasnika) {
        this.predstavnikSuvlasnika = predstavnikSuvlasnika;
    }

    public Float getPricuvaPoM2() {
        return pricuvaPoM2;
    }

    public void setPricuvaPoM2(Float pricuvaPoM2) {
        this.pricuvaPoM2 = pricuvaPoM2;
    }

    public Float getPovrsinaZgradeM2() {
        return povrsinaZgradeM2;
    }

    public void setPovrsinaZgradeM2(Float povrsinaZgradeM2) {
        this.povrsinaZgradeM2 = povrsinaZgradeM2;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public Long getIdZgrade() {
        return idZgrade;
    }

    public void setIdZgrade(Long idZgrade) {
        this.idZgrade = idZgrade;
    }

    public String getNazivZgrade() {
        return nazivZgrade;
    }

    public void setNazivZgrade(String nazivZgrade) {
        this.nazivZgrade = nazivZgrade;
    }

    public String getUlicaBroj() {
        return ulicaBroj;
    }

    public void setUlicaBroj(String ulicaBroj) {
        this.ulicaBroj = ulicaBroj;
    }

    public String getBrojPoste() {
        return brojPoste;
    }

    public void setBrojPoste(String brojPoste) {
        this.brojPoste = brojPoste;
    }

    public String getMjesto() {
        return mjesto;
    }

    public void setMjesto(String mjesto) {
        this.mjesto = mjesto;
    }
}
