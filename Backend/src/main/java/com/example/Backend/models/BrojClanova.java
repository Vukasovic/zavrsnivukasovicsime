package com.example.Backend.models;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name="brojClanova", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"prostor_id_prostor","godina","mjesec"})
})
public class BrojClanova {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int godina;
    private int mjesec;
    private Float brojClanova;
    private Float pstOdUkupno;
    private String napomena;

    @NotNull
    @ManyToOne
    private Prostor prostor;

    public BrojClanova(){}

    public BrojClanova(Prostor prostor, int godina, int mjesec, Float brojClanova, Float pstOdUkupno, String napomena){

        this.prostor = prostor;
        this.godina = godina;
        this.mjesec = mjesec;
        this.brojClanova = brojClanova;
        this.pstOdUkupno = pstOdUkupno;
        this.napomena = napomena;
    }

    public int getGodina() {
        return godina;
    }

    public int getMjesec() {
        return mjesec;
    }

    public Float getBrojClanova() {
        return brojClanova;
    }

    public void setBrojClanova(Float brojClanova) {
        this.brojClanova = brojClanova;
    }

    public Float getPstOdUkupno() {
        return pstOdUkupno;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public Prostor getProstor() {
        return prostor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setGodina(int godina) {
        this.godina = godina;
    }

    public void setMjesec(int mjesec) {
        this.mjesec = mjesec;
    }

    public void setPstOdUkupno(Float pstOdUkupno) {
        this.pstOdUkupno = pstOdUkupno;
    }

    public void setProstor(Prostor prostor) {
        this.prostor = prostor;
    }
}
