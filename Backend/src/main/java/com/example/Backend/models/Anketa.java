package com.example.Backend.models;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="anketa")
public class Anketa {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idAnketa;

    @NotNull
    @ManyToOne
    private Zgrada zgrada;

    private String nazivAnkete;
    private String tekstAnkete;
    private Date dtPocetak;
    private Date dtKraj;
    private String rezultat;

    public Anketa(){}

    public Anketa(Zgrada zgrada, String nazivAnkete, String tekstAnkete, Date dtPocetak, Date dtKraj){

        this.zgrada = zgrada;
        this.nazivAnkete = nazivAnkete;
        this.tekstAnkete = tekstAnkete;
        this.dtPocetak = dtPocetak;
        this.dtKraj = dtKraj;
        this.rezultat = null;
    }

    public Long getIdAnketa() {
        return idAnketa;
    }

    public String getNazivAnkete() {
        return nazivAnkete;
    }

    public String getTekstAnkete() {
        return tekstAnkete;
    }

    public Date getDtPocetak() {
        return dtPocetak;
    }

    public Date getDtKraj() {
        return dtKraj;
    }

    public String getRezultat() {
        return rezultat;
    }

    public void setRezultat(String r) {
        this.rezultat = r;
    }

    public Zgrada getZgrada() {
        return zgrada;
    }

    public void setZgrada(Zgrada zgrada) {
        this.zgrada = zgrada;
    }
}
