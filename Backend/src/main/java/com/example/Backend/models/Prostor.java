package com.example.Backend.models;

import com.example.Backend.rest.DTO.ProstorDTO;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.Optional;

@Entity
@Table(name="prostor")
public class Prostor {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idProstor;

    @NotNull
    @ManyToOne
    private Zgrada zgrada;

    @NotNull
    @ManyToOne
    private VrstaProstora vrstaProstora;

    private String brUlaza;
    private String kat;
    private String brStana;
    private String sifObjektaUpravitelj;
    private String sifObjektaViO;
    private String sifObjektaToplana;
    private String vlasnik;
    private String vlasnikTel;
    private String vlasnikEmail;
    private String racunImeViO;
    private String racunImeToplana;
    private String napomena;
    private Float povrsinaM2;
    private String pswVlasnik;

    public Prostor(){}

    public Prostor(Optional<Zgrada> zgrada, Optional<VrstaProstora> vrstaProstora, String brUlaza, String kat, String brStana, String sifObjektaUpravitelj, String sifObjektaViO,
                   String sifObjektaToplana, String vlasnik, String vlasnikTel, String vlasnikEmail, String racunImeViO, String racunImeToplana, String napomena,
                   Float povrsinaM2, String pswVlasnik){

        this.zgrada = zgrada.get();
        this.vrstaProstora = vrstaProstora.get();
        this.brUlaza = brUlaza;
        this.kat = kat;
        this.brStana = brStana;
        this.sifObjektaUpravitelj = sifObjektaUpravitelj;
        this.sifObjektaViO = sifObjektaViO;
        this.sifObjektaToplana = sifObjektaToplana;
        this.vlasnik = vlasnik;
        this.vlasnikTel = vlasnikTel;
        this.vlasnikEmail = vlasnikEmail;
        this.racunImeViO = racunImeViO;
        this.racunImeToplana = racunImeToplana;
        this.napomena = napomena;
        this.povrsinaM2 = povrsinaM2;
        this.pswVlasnik = pswVlasnik;
    }

    public Zgrada getZgrada() {
        return zgrada;
    }

    public void setZgrada(Zgrada zgrada) {
        this.zgrada = zgrada;
    }

    public Long getIdProstor() {
        return idProstor;
    }

    public VrstaProstora getVrstaProstora() {
        return vrstaProstora;
    }

    public void setVrstaProstora(VrstaProstora vrstaProstora) {
        this.vrstaProstora = vrstaProstora;
    }

    public String getBrUlaza() {
        return brUlaza;
    }

    public String getKat() {
        return kat;
    }

    public String getBrStana() {
        return brStana;
    }

    public String getSifObjektaUpravitelj() {
        return sifObjektaUpravitelj;
    }

    public String getSifObjektaViO() {
        return sifObjektaViO;
    }

    public String getSifObjektaToplana() {
        return sifObjektaToplana;
    }

    public String getVlasnik() {
        return vlasnik;
    }

    public void setVlasnik(String vlasnik) {
        this.vlasnik = vlasnik;
    }

    public String getVlasnikTel() {
        return vlasnikTel;
    }

    public void setVlasnikTel(String vlasnikTel) {
        this.vlasnikTel = vlasnikTel;
    }

    public String getVlasnikEmail() {
        return vlasnikEmail;
    }

    public void setVlasnikEmail(String vlasnikEmail) {
        this.vlasnikEmail = vlasnikEmail;
    }

    public String getRacunImeViO() {
        return racunImeViO;
    }

    public void setRacunImeViO(String racunImeViO) {
        this.racunImeViO = racunImeViO;
    }

    public String getRacunImeToplana() {
        return racunImeToplana;
    }

    public void setRacunImeToplana(String racunImeToplana) {
        this.racunImeToplana = racunImeToplana;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public Float getPovrsinaM2() {
        return povrsinaM2;
    }

    public String getPswVlasnik() {
        return pswVlasnik;
    }

    public void setPswVlasnik(String pswVlasnik) {
        this.pswVlasnik = pswVlasnik;
    }
}
