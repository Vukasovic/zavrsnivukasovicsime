package com.example.Backend.models;

import ch.qos.logback.core.joran.spi.NoAutoStart;
import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name="anketaOdgovor", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"anketa_id_anketa","odgovor"})
}
)
public class AnketaOdgovor {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne
    private Anketa anketa;
    private String odgovor;

    public AnketaOdgovor(){}

    public AnketaOdgovor(Anketa anketa, String odgovor){

        this.anketa = anketa;
        this.odgovor = odgovor;
    }

    public Anketa getAnketa() {
        return anketa;
    }

    public String getOdgovor() {
        return odgovor;
    }
}
