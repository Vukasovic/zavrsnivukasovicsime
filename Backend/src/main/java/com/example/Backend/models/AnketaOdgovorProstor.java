package com.example.Backend.models;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "anketaOdgoroProstor", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"anketa_id_anketa", "prostor_id_prostor"})
})
public class AnketaOdgovorProstor {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne
    private Anketa anketa;

    @NotNull
    @ManyToOne
    private Prostor prostor;

    private String odgovor;
    private String odgovorTekst;

    public AnketaOdgovorProstor(){}

    public AnketaOdgovorProstor(Anketa anketa, Prostor prostor, String odgovor, String odgovorTekst){

        this.anketa = anketa;
        this.prostor = prostor;
        this.odgovor = odgovor;
        this.odgovorTekst = odgovorTekst;
    }

    public Anketa getAnketa() {
        return anketa;
    }

    public void setAnketa(Anketa anketa) {
        this.anketa = anketa;
    }

    public Prostor getProstor() {
        return prostor;
    }

    public void setProstor(Prostor prostor) {
        this.prostor = prostor;
    }

    public String getOdgovor() {
        return odgovor;
    }

    public void setOdgovor(String odgovor) {
        this.odgovor = odgovor;
    }

    public String getOdgovorTekst() {
        return odgovorTekst;
    }

    public void setOdgovorTekst(String odgovorTekst) {
        this.odgovorTekst = odgovorTekst;
    }
}
