package com.example.Backend.models;

import javax.persistence.*;

@Entity
@Table(name="vrstaProstora")
public class VrstaProstora {
    @Id
    @Column( unique = true, nullable = false)
    private String idVrstaProstora;

    private Float koefPricuve;
    private String napomena;

    public VrstaProstora(){}

    public VrstaProstora(Float koefPricuve, String napomena){

        this.koefPricuve = koefPricuve;
        this.napomena = napomena;
    }

    public Float getKoefPricuve() {
        return koefPricuve;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public String getIdVrstaProstora() {
        return idVrstaProstora;
    }
}
