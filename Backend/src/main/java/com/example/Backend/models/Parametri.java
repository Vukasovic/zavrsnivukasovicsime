package com.example.Backend.models;

import com.sun.istack.NotNull;
import sun.util.resources.cldr.ga.TimeZoneNames_ga;

import javax.persistence.*;

@Entity
@Table(name = "parametri", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"zgrada_id_zgrade"})
})
public class Parametri {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne
    private Zgrada zgrada;

    private int unosClanovaDoDanauMjesecu;
    private String pswEditZgrada;

    public Parametri(){}

    public Parametri(Zgrada zgrada, int unosClanovaDoDanauMjesecu, String pswEditZgrada){

        this.zgrada = zgrada;
        this.unosClanovaDoDanauMjesecu = unosClanovaDoDanauMjesecu;
        this.pswEditZgrada = pswEditZgrada;
    }

    public Zgrada getZgrada() {
        return zgrada;
    }

    public int getUnosClanovaDoDanauMjesecu() {
        return unosClanovaDoDanauMjesecu;
    }

    public String getPswEditZgrada() {
        return pswEditZgrada;
    }

    public void setPswEditZgrada(String pswEditZgrada) {
        this.pswEditZgrada = pswEditZgrada;
    }
}
