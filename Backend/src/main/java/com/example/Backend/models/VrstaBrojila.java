package com.example.Backend.models;

import javax.persistence.*;

@Entity
@Table(name = "vrstaBrojila")
public class VrstaBrojila {

    @Id
    @Column(unique = true, nullable = false)
    private String idVrstaBrojila;

    private String napomena;

    public VrstaBrojila(){}

    public VrstaBrojila(String idVrstaBrojila, String napomena){

        this.idVrstaBrojila = idVrstaBrojila;
        this.napomena = napomena;
    }

    public String getIdVrstaBrojila() {
        return idVrstaBrojila;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }
}
