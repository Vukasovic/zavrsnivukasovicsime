package com.example.Backend.service.impl;

import com.example.Backend.DAO.ZgradaRepository;
import com.example.Backend.models.Zgrada;
import com.example.Backend.service.IZgradaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ZgradaService implements IZgradaService {

    private ZgradaRepository repo;

    @Autowired
    public ZgradaService(ZgradaRepository repo) {
        this.repo = repo;
    }

    @Override
    public Zgrada saveZgrada(Zgrada z) {
        return repo.save(z);
    }

    @Override
    public List<Zgrada> listAll() {
        return repo.findAll();
    }

    @Override
    public Optional<Zgrada> findById(Long id) {
        return repo.findById(id);
    }
}
