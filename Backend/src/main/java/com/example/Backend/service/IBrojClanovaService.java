package com.example.Backend.service;

import com.example.Backend.models.BrojClanova;
import com.example.Backend.models.Prostor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IBrojClanovaService {
    public BrojClanova saveBrojClanova(BrojClanova brC);
    public List<BrojClanova> listAll();
    public List<BrojClanova> findByProstor(Prostor p);
}
