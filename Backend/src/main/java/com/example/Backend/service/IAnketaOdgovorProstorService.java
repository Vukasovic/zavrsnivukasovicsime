package com.example.Backend.service;

import com.example.Backend.models.Anketa;
import com.example.Backend.models.AnketaOdgovorProstor;
import com.example.Backend.models.Prostor;
import com.example.Backend.models.Zgrada;
import com.sun.webkit.perf.PerfLogger;
import org.springframework.stereotype.Service;

import javax.persistence.SecondaryTable;
import java.util.List;

@Service
public interface IAnketaOdgovorProstorService {

    public AnketaOdgovorProstor saveAnketaOdgovor(AnketaOdgovorProstor aop);

    public List<AnketaOdgovorProstor> listAll();

//    public List<AnketaOdgovorProstor> findByAnketa(Anketa anketa);

    public List<AnketaOdgovorProstor> findByProstor(Prostor prostor);

    public List<AnketaOdgovorProstor> findByZgrada(Zgrada zgrada);
}
