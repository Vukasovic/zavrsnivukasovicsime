package com.example.Backend.service.impl;

import com.example.Backend.DAO.VrstaProstoraRepository;
import com.example.Backend.models.VrstaProstora;
import com.example.Backend.models.Zgrada;
import com.example.Backend.service.IVrstaProstoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VrstaProstoraService implements IVrstaProstoraService {

    private VrstaProstoraRepository repo;

    @Autowired
    public VrstaProstoraService(VrstaProstoraRepository repo){
        this.repo = repo;
    }

    @Override
    public List<VrstaProstora> listAll() {
        return repo.findAll();
    }

    @Override
    public Optional<VrstaProstora> findById(String id) {
        return repo.findById(id);
    }

}
