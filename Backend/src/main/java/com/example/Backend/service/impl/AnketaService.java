package com.example.Backend.service.impl;

import com.example.Backend.DAO.AnketaRepository;
import com.example.Backend.models.Anketa;
import com.example.Backend.models.Zgrada;
import com.example.Backend.service.IAnketaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AnketaService implements IAnketaService {

    private AnketaRepository repo;

    @Autowired
    public  AnketaService(AnketaRepository repo){
        this.repo = repo;
    }

    @Override
    public Anketa saveAnketa(Anketa a) {
        return repo.save(a);
    }

    @Override
    public List<Anketa> listAll() {
        return repo.findAll();
    }

    @Override
    public List<Anketa> findByZgrada(Zgrada zgrada) {
        return repo.findByZgrada(zgrada);
    }

//    @Override
//    public List<Anketa> findActive(Zgrada zgrada, Date datum) {
//        return listAll().stream().filter(a -> {
//            return (a.getZgrada() == zgrada) && (a.getDtKraj().compareTo(datum) > 0);
//        }).collect(Collectors.toList());
//    }

    @Override
    public Optional<Anketa> findById(Long id) {
        return repo.findById(id);
    }
}
