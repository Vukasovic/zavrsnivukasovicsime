package com.example.Backend.service;

import com.example.Backend.models.Anketa;
import com.example.Backend.models.AnketaOdgovor;
import com.example.Backend.models.Zgrada;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IAnketaOdgovorService {

    public AnketaOdgovor saveAnketaOdgovor(AnketaOdgovor ao);

    public List<AnketaOdgovor> listAll();

//    public List<AnketaOdgovor> findByAnketa(Anketa anketa);

    public List<AnketaOdgovor> findByZgrada(Zgrada zgrada);
}
