package com.example.Backend.service.impl;

import com.example.Backend.DAO.ProstorRepository;
import com.example.Backend.models.Prostor;
import com.example.Backend.models.Zgrada;
import com.example.Backend.service.IProstorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProstorService implements IProstorService {

    private ProstorRepository repo;

    @Autowired
    public ProstorService(ProstorRepository repo) {
        this.repo = repo;
    }

    @Override
    public Prostor saveProstor(Prostor p) {
        return repo.save(p);
    }

    @Override
    public List<Prostor> listAll() {
        return repo.findAll();
    }

    @Override
    public List<Prostor> findByZgrada(Zgrada z) {
        return repo.findByZgrada(z);
    }

    @Override
    public Optional<Prostor> findById(Long id) {
        return repo.findById(id);
    }

    @Override
    public Optional<Prostor> findByVlasnikEmail(String email) {

        return repo.findByVlasnikEmail(email);
    }
}
