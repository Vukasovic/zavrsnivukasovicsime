package com.example.Backend.service;

import com.example.Backend.models.VrstaProstora;
import com.example.Backend.models.Zgrada;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface IVrstaProstoraService {
    public List<VrstaProstora> listAll();
    public Optional<VrstaProstora> findById(String id);
}
