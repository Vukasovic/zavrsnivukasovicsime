package com.example.Backend.service.impl;

import com.example.Backend.DAO.AnketaOdgovorProstorRepository;
import com.example.Backend.models.Anketa;
import com.example.Backend.models.AnketaOdgovorProstor;
import com.example.Backend.models.Prostor;
import com.example.Backend.models.Zgrada;
import com.example.Backend.service.IAnketaOdgovorProstorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AnketaOdgovorProstorService implements IAnketaOdgovorProstorService {


    private AnketaOdgovorProstorRepository repo;

    @Autowired
    public AnketaOdgovorProstorService(AnketaOdgovorProstorRepository repo){ this.repo = repo; }

    @Override
    public AnketaOdgovorProstor saveAnketaOdgovor(AnketaOdgovorProstor aop) {
        return repo.save(aop);
    }

    @Override
    public List<AnketaOdgovorProstor> listAll() {
        return repo.findAll();
    }

//    @Override
//    public List<AnketaOdgovorProstor> findByAnketa(Anketa anketa) {
//        return listAll().stream().filter(a -> a.getAnketa() == anketa).collect(Collectors.toList());
//    }

    @Override
    public List<AnketaOdgovorProstor> findByProstor(Prostor prostor) {
        return repo.findByProstor(prostor);
    }

    @Override
    public List<AnketaOdgovorProstor> findByZgrada(Zgrada zgrada) {
        return listAll().stream().filter(a -> a.getProstor().getZgrada() == zgrada).collect(Collectors.toList());
    }
}
