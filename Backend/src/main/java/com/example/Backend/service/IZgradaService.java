package com.example.Backend.service;

import com.example.Backend.models.Zgrada;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface IZgradaService {
    public Zgrada saveZgrada(Zgrada z);
    public List<Zgrada> listAll();
    public Optional<Zgrada> findById(Long id);
}
