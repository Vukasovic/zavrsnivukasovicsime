package com.example.Backend.service.impl;

import com.example.Backend.DAO.ObavijestRepository;
import com.example.Backend.models.Obavijest;
import com.example.Backend.models.Zgrada;
import com.example.Backend.service.IObavijestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ObavijestService implements IObavijestService {

    private ObavijestRepository repo;

    @Autowired
    public ObavijestService(ObavijestRepository repo) {
        this.repo = repo;
    }

    @Override
    public Obavijest saveObavijest(Obavijest o) {
        return repo.save(o);
    }

    @Override
    public List<Obavijest> listAll() {
        return repo.findAll();
    }

    @Override
    public List<Obavijest> findByZgrada(Zgrada zgrada) {
        return repo.findByZgrada(zgrada);
    }
}
