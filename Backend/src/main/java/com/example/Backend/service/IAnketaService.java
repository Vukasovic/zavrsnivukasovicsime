package com.example.Backend.service;

import com.example.Backend.models.Anketa;
import com.example.Backend.models.Zgrada;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Service
public interface IAnketaService {

    public Anketa saveAnketa(Anketa a);

    public List<Anketa> listAll();

    public List<Anketa> findByZgrada(Zgrada zgrada);

//    public List<Anketa> findActive(Zgrada zgrada, Date datum);

    public Optional<Anketa> findById(Long id);
}
