package com.example.Backend.service;

import com.example.Backend.models.Obavijest;
import com.example.Backend.models.Zgrada;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IObavijestService {

    public Obavijest saveObavijest(Obavijest o);

    public List<Obavijest> listAll();

    public List<Obavijest> findByZgrada(Zgrada zgrada);


}
