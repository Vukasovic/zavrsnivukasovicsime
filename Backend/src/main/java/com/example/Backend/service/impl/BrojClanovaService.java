package com.example.Backend.service.impl;

import com.example.Backend.DAO.BrojClanovaRepository;
import com.example.Backend.models.BrojClanova;
import com.example.Backend.models.Prostor;
import com.example.Backend.service.IBrojClanovaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BrojClanovaService implements IBrojClanovaService {


    private BrojClanovaRepository repo;

    @Autowired
    public BrojClanovaService(BrojClanovaRepository repo) {
        this.repo = repo;
    }

    @Override
    public BrojClanova saveBrojClanova(BrojClanova brC) {
        return repo.save(brC);
    }

    @Override
    public List<BrojClanova> listAll() {
        return repo.findAll();
    }

    @Override
    public List<BrojClanova> findByProstor(Prostor p) {
        return repo.findByProstor(p);
    }
}
