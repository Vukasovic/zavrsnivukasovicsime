package com.example.Backend.service.impl;

import com.example.Backend.DAO.AnketaOdgovorRepository;
import com.example.Backend.models.Anketa;
import com.example.Backend.models.AnketaOdgovor;
import com.example.Backend.models.Zgrada;
import com.example.Backend.service.IAnketaOdgovorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AnketaOdgovorService implements IAnketaOdgovorService {

    private AnketaOdgovorRepository repo;

    @Autowired
    public AnketaOdgovorService(AnketaOdgovorRepository repo){ this.repo = repo; }

    @Override
    public AnketaOdgovor saveAnketaOdgovor(AnketaOdgovor ao) {
        return repo.save(ao);
    }

    @Override
    public List<AnketaOdgovor> listAll() {
        return repo.findAll();
    }

//    @Override
//    public List<AnketaOdgovor> findByAnketa(Anketa anketa) {
//        return listAll().stream().filter(a -> a.getAnketa() == anketa).collect(Collectors.toList());
//    }

    @Override
    public List<AnketaOdgovor> findByZgrada(Zgrada zgrada) {
        return listAll().stream().filter(a -> a.getAnketa().getZgrada() == zgrada).collect(Collectors.toList());
    }
}
