package com.example.Backend.service;

import com.example.Backend.models.Prostor;
import com.example.Backend.models.Zgrada;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface IProstorService {
    public Prostor saveProstor(Prostor p);
    public List<Prostor> listAll();
    public List<Prostor> findByZgrada(Zgrada z);
    public Optional<Prostor> findById(Long id);
    public Optional<Prostor> findByVlasnikEmail (String email);
}
