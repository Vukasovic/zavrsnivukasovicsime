package com.example.Backend.rest.DTO;

import com.example.Backend.models.AnketaOdgovorProstor;

public class AnketaOdgovorProstorDTO {

    private int idAnketa;
    private int idProstor;
    private String odgovor = null;
    private String odgovorTekst = null;

    public AnketaOdgovorProstorDTO( int idAnketa, int idProstor, String odgovor, String odgovorTekst){
        this.idAnketa = idAnketa;
        this.idProstor = idProstor;
        this.odgovor = odgovor;
        this.odgovorTekst = odgovorTekst;
    }

    public AnketaOdgovorProstorDTO(AnketaOdgovorProstor aop){

        this.idAnketa = Math.toIntExact(aop.getAnketa().getIdAnketa());
        this.idProstor = Math.toIntExact(aop.getProstor().getIdProstor());
        this.odgovor = aop.getOdgovor();
        this.odgovorTekst = aop.getOdgovorTekst();
    }

    public int getIdAnketa() {
        return idAnketa;
    }

    public void setIdAnketa(int idAnketa) {
        this.idAnketa = idAnketa;
    }

    public int getIdProstor() {
        return idProstor;
    }

    public void setIdProstor(int idProstor) {
        this.idProstor = idProstor;
    }

    public String getOdgovor() {
        return odgovor;
    }

    public void setOdgovor(String odgovor) {
        this.odgovor = odgovor;
    }

    public String getOdgovorTekst() {
        return odgovorTekst;
    }

    public void setOdgovorTekst(String odgovorTekst) {
        this.odgovorTekst = odgovorTekst;
    }
}
