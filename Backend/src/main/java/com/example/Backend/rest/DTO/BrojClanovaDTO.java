package com.example.Backend.rest.DTO;

import com.example.Backend.models.BrojClanova;

public class BrojClanovaDTO {

    private int idProstor;
    private int godina;
    private int mjesec;
    private Float brojClanova;
    private Float pstOdUkupno;
    private String napomena;

    public BrojClanovaDTO(){}

    public BrojClanovaDTO (BrojClanova brc){

        this.idProstor = Math.toIntExact(brc.getProstor().getIdProstor());
        this.godina = brc.getGodina();
        this.mjesec = brc.getMjesec();
        this.brojClanova = brc.getBrojClanova();
        this.pstOdUkupno = brc.getPstOdUkupno();
        this.napomena = brc.getNapomena();
    }

    public int getGodina() {
        return godina;
    }

    public void setGodina(int godina) {
        this.godina = godina;
    }

    public int getMjesec() {
        return mjesec;
    }

    public void setMjesec(int mjesec) {
        this.mjesec = mjesec;
    }

    public Float getBrojClanova() {
        return brojClanova;
    }

    public void setBrojClanova(Float brojClanova) {
        this.brojClanova = brojClanova;
    }

    public Float getPstOdUkupno() {
        return pstOdUkupno;
    }

    public void setPstOdUkupno(Float pstOdUkupno) {
        this.pstOdUkupno = pstOdUkupno;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public int getIdProstor() {
        return idProstor;
    }

    public void setIdProstor(int idProstor) {
        this.idProstor = idProstor;
    }
}
