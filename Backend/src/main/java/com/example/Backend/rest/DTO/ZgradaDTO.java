package com.example.Backend.rest.DTO;


import com.example.Backend.models.Zgrada;

public class ZgradaDTO {

    private int idZgrade = 0;

    private String nazivZgrade = null;
    private String ulicaBroj = null;
    private String brojPoste = null;
    private String mjesto = null;
    private String upraviteljZgrade = null;
    private String predstavnikSuvlasnika = null;
    private Float pricuvaPoM2 = null;
    private Float povrsinaZgradeM2 = null;
    private String napomena = null;
    public ZgradaDTO(Zgrada zgrada){
        this.idZgrade = Math.toIntExact(zgrada.getIdZgrade());
        this.ulicaBroj = zgrada.getUlicaBroj();
        this.brojPoste = zgrada.getBrojPoste();
        this.mjesto = zgrada.getMjesto();
        this.nazivZgrade = zgrada.getNazivZgrade();
        this.upraviteljZgrade = zgrada.getUpraviteljZgrade();
        this.predstavnikSuvlasnika = zgrada.getPredstavnikSuvlasnika();
        this.pricuvaPoM2 = zgrada.getPricuvaPoM2();
        this.povrsinaZgradeM2 = zgrada.getPovrsinaZgradeM2();
        this.napomena = zgrada.getNapomena();
    }

    public int getIdZgrade() {
        return idZgrade;
    }

    public void setIdZgrade(int idZgrade) {
        this.idZgrade = idZgrade;
    }

    public String getNazivZgrade() {
        return nazivZgrade;
    }

    public void setNazivZgrade(String nazivZgrade) {
        this.nazivZgrade = nazivZgrade;
    }

    public String getUlicaBroj() {
        return ulicaBroj;
    }

    public void setUlicaBroj(String ulicaBroj) {
        this.ulicaBroj = ulicaBroj;
    }

    public String getBrojPoste() {
        return brojPoste;
    }

    public void setBrojPoste(String brojPoste) {
        this.brojPoste = brojPoste;
    }

    public String getMjesto() {
        return mjesto;
    }

    public void setMjesto(String mjesto) {
        this.mjesto = mjesto;
    }

    public String getUpraviteljZgrade() {
        return upraviteljZgrade;
    }

    public void setUpraviteljZgrade(String upraviteljZgrade) {
        this.upraviteljZgrade = upraviteljZgrade;
    }

    public String getPredstavnikSuvlasnika() {
        return predstavnikSuvlasnika;
    }

    public void setPredstavnikSuvlasnika(String predstavnikSuvlasnika) {
        this.predstavnikSuvlasnika = predstavnikSuvlasnika;
    }

    public Float getPricuvaPoM2() {
        return pricuvaPoM2;
    }

    public void setPricuvaPoM2(Float pricuvaPoM2) {
        this.pricuvaPoM2 = pricuvaPoM2;
    }

    public Float getPovrsinaZgradeM2() {
        return povrsinaZgradeM2;
    }

    public void setPovrsinaZgradeM2(Float povrsinaZgradeM2) {
        this.povrsinaZgradeM2 = povrsinaZgradeM2;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }
}
