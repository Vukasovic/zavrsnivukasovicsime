package com.example.Backend.rest.DTO;

import com.example.Backend.models.AnketaOdgovor;

public class AnketaOdgovorDTO {

    private int idAnketa;
    private String odgovor = null;

    public AnketaOdgovorDTO(
            int idAnketa,
            String odgovor
    ){
        this.idAnketa =idAnketa;
        this.odgovor =odgovor;
    }

    public AnketaOdgovorDTO(AnketaOdgovor ao){

        this.idAnketa = Math.toIntExact(ao.getAnketa().getIdAnketa());
        this.odgovor = ao.getOdgovor();
    }

    public int getIdAnketa() {
        return idAnketa;
    }

    public void setIdAnketa(int idAnketa) {
        this.idAnketa = idAnketa;
    }

    public String getOdgovor() {
        return odgovor;
    }

    public void setOdgovor(String odgovor) {
        this.odgovor = odgovor;
    }
}
