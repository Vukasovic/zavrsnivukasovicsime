package com.example.Backend.rest.DTO;

import com.example.Backend.models.Obavijest;
import com.example.Backend.models.Zgrada;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

//@Component
public class ObavijestDTO {

    private int idZgrade;
    private Date datumVrijeme;
    private String naslovObavijesti;
    private String obavijest;

    public ObavijestDTO(
            int idZgrade,
            Date datumVrijeme,
            String naslovObavijesti,
            String obavijest
    ){
        this.idZgrade = idZgrade;
        this.datumVrijeme = datumVrijeme;
        this.naslovObavijesti =naslovObavijesti;
        this.obavijest = obavijest;
    }

    public ObavijestDTO(Obavijest o){

        this.idZgrade = Math.toIntExact(o.getZgrada().getIdZgrade());
        this.datumVrijeme = new Date(o.getDatumVrijeme().getTime());
        this.naslovObavijesti = o.getNaslovObavijesti();
        this.obavijest = o.getObavijest();
    }

    public int getIdZgrade() {
        return idZgrade;
    }

    public void setIdZgrade(int idZgrade) {
        this.idZgrade = idZgrade;
    }

    public Date getDatumVrijeme() {
        return datumVrijeme;
    }

    public void setDatumVrijeme(Timestamp datumVrijeme) {
        this.datumVrijeme = datumVrijeme;
    }

    public String getNaslovObavijesti() {
        return naslovObavijesti;
    }

    public void setNaslovObavijesti(String naslovObavijesti) {
        this.naslovObavijesti = naslovObavijesti;
    }

    public String getObavijest() {
        return obavijest;
    }

    public void setObavijest(String obavijest) {
        this.obavijest = obavijest;
    }
}
