package com.example.Backend.rest.DTO;

import com.example.Backend.models.Prostor;

public class ProstorDTO {

    private int idProstor = 0;
    private int idZgrade = 0;
    private String idVrstaProstora = null;
    private String brUlaza = null;
    private String kat = null;
    private String brStana = null;
    private String sifObjektaUpravitelj = null;
    private String sifObjektaViO = null;
    private String sifObjektaToplana = null;
    private String vlasnik = null;
    private String vlasnikTel = null;
    private String vlasnikEmail = null;
    private String racunImeViO = null;
    private String racunImeToplana = null;
    private String napomena = null;
    private Float povrsinaM2 = null;
    private String pswVlasnik = null;


    public ProstorDTO(int idProstor,int idZgrade,
            String idVrstaProstora,
            String brUlaza,
            String kat,
            String brStana,
            String sifObjektaUpravitelj,
            String sifObjektaViO,
            String sifObjektaToplana,
            String vlasnik,
            String vlasnikTel,
            String vlasnikEmail,
            String racunImeViO,
            String racunImeToplana,
            String napomena,
            Float povrsinaM2,
             String pswVlasnik
    ){
        this.idProstor = Math.toIntExact(idProstor);
        this.idZgrade = Math.toIntExact(idZgrade);
        this.idVrstaProstora =idVrstaProstora;
        this.brUlaza = brUlaza;
        this.kat =kat;
        this.brStana = brStana;
        this.sifObjektaUpravitelj = sifObjektaUpravitelj;
        this.sifObjektaViO = sifObjektaViO;
        this.sifObjektaToplana = sifObjektaToplana;
        this.vlasnik = vlasnik;
        this.vlasnikEmail = vlasnikEmail;
        this.vlasnikTel = vlasnikTel;
        this.racunImeViO = racunImeViO;
        this.racunImeToplana = racunImeToplana;
        this.napomena = napomena;
        this.povrsinaM2 = povrsinaM2;
        this.pswVlasnik = pswVlasnik;

    }

    public ProstorDTO(Prostor p){
        this.idProstor = Math.toIntExact(p.getIdProstor());
        this.idZgrade = Math.toIntExact(p.getZgrada().getIdZgrade());
        this.idVrstaProstora = p.getVrstaProstora().getIdVrstaProstora();
        this.brUlaza = p.getBrUlaza();
        this.kat = p.getKat();
        this.brStana = p.getBrStana();
        this.sifObjektaUpravitelj = p.getSifObjektaUpravitelj();
        this.sifObjektaViO = p.getSifObjektaViO();
        this.sifObjektaToplana = p.getSifObjektaToplana();
        this.vlasnik = p.getVlasnik();
        this.vlasnikEmail = p.getVlasnikEmail();
        this.vlasnikTel = p.getVlasnikTel();
        this.racunImeViO = p.getRacunImeViO();
        this.racunImeToplana = p.getRacunImeToplana();
        this.napomena = p.getNapomena();
        this.povrsinaM2 = p.getPovrsinaM2();
        this.pswVlasnik = p.getPswVlasnik();

    }

    public int getIdProstor() {
        return idProstor;
    }

    public void setIdProstor(int idProstor) {
        this.idProstor = idProstor;
    }

    public int getIdZgrade() {
        return idZgrade;
    }

    public void setIdZgrade(int idZgrade) {
        this.idZgrade = idZgrade;
    }

    public String getIdVrstaProstora() {
        return idVrstaProstora;
    }

    public void setIdVrstaProstora(String idVrstaProstora) {
        this.idVrstaProstora = idVrstaProstora;
    }

    public String getBrUlaza() {
        return brUlaza;
    }

    public void setBrUlaza(String brUlaza) {
        this.brUlaza = brUlaza;
    }

    public String getKat() {
        return kat;
    }

    public void setKat(String kat) {
        this.kat = kat;
    }

    public String getBrStana() {
        return brStana;
    }

    public void setBrStana(String brStana) {
        this.brStana = brStana;
    }

    public String getSifObjektaUpravitelj() {
        return sifObjektaUpravitelj;
    }

    public void setSifObjektaUpravitelj(String sifObjektaUpravitelj) {
        this.sifObjektaUpravitelj = sifObjektaUpravitelj;
    }

    public String getSifObjektaViO() {
        return sifObjektaViO;
    }

    public void setSifObjektaViO(String sifObjektaViO) {
        this.sifObjektaViO = sifObjektaViO;
    }

    public String getSifObjektaToplana() {
        return sifObjektaToplana;
    }

    public void setSifObjektaToplana(String sifObjektaToplana) {
        this.sifObjektaToplana = sifObjektaToplana;
    }

    public String getVlasnik() {
        return vlasnik;
    }

    public void setVlasnik(String vlasnik) {
        this.vlasnik = vlasnik;
    }

    public String getVlasnikTel() {
        return vlasnikTel;
    }

    public void setVlasnikTel(String vlasnikTel) {
        this.vlasnikTel = vlasnikTel;
    }

    public String getVlasnikEmail() {
        return vlasnikEmail;
    }

    public void setVlasnikEmail(String vlasnikEmail) {
        this.vlasnikEmail = vlasnikEmail;
    }

    public String getRacunImeViO() {
        return racunImeViO;
    }

    public void setRacunImeViO(String racunImeViO) {
        this.racunImeViO = racunImeViO;
    }

    public String getRacunImeToplana() {
        return racunImeToplana;
    }

    public void setRacunImeToplana(String racunImeToplana) {
        this.racunImeToplana = racunImeToplana;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public Float getPovrsinaM2() {
        return povrsinaM2;
    }

    public void setPovrsinaM2(Float povrsinaM2) {
        this.povrsinaM2 = povrsinaM2;
    }

    public String getPswVlasnik(){
        return pswVlasnik;
    }

    public void setPswVlasnik(String pswVlasnik) {
        this.pswVlasnik = pswVlasnik;
    }
}
