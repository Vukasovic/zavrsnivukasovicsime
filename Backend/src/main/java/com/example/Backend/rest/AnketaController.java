package com.example.Backend.rest;

import com.example.Backend.models.Anketa;
import com.example.Backend.models.AnketaOdgovor;
import com.example.Backend.models.AnketaOdgovorProstor;
import com.example.Backend.rest.DTO.AnketaDTO;
import com.example.Backend.rest.DTO.AnketaOdgovorDTO;
import com.example.Backend.rest.DTO.AnketaOdgovorProstorDTO;
import com.example.Backend.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/anketa")
public class AnketaController {

    private final String SEPARATOR = "$$$";

    private IAnketaService aServ;
    private IZgradaService zServ;
    private IAnketaOdgovorService aoServ;
    private IAnketaOdgovorProstorService aopServ;
    private IProstorService pServ;

    @Autowired
    public AnketaController(IAnketaService aServ, IZgradaService zServ, IAnketaOdgovorService aoServ, IAnketaOdgovorProstorService aopServ, IProstorService pServ){

        this.aServ = aServ;
        this.zServ = zServ;
        this.aoServ = aoServ;
        this.aopServ = aopServ;
        this.pServ = pServ;
    }

    @GetMapping("/get/{id}")
    @ResponseBody
    public List<AnketaDTO> getAnkete (@PathVariable int id){
        List<Anketa> ankete = aServ.findByZgrada(zServ.findById(new Long(id)).get());
        List<AnketaDTO> anketeDto = new ArrayList<>();
        for (Anketa a: ankete){
            anketeDto.add(new AnketaDTO(a));
        }
        return  anketeDto;
    }

    @GetMapping("/odgovori/{id}")
    @ResponseBody
    public List<AnketaOdgovorDTO> getOdgovori (@PathVariable int id){
        List<AnketaOdgovor> odgovori = aoServ.findByZgrada(zServ.findById(new Long(id)).get());
        List<AnketaOdgovorDTO> odgovoriDto = new ArrayList<>();
        for (AnketaOdgovor o: odgovori){
            odgovoriDto.add(new AnketaOdgovorDTO(o));
        }
        return odgovoriDto;
    }
    @GetMapping("/prostori/{id}")
    @ResponseBody
    public List<AnketaOdgovorProstorDTO> getProstori (@PathVariable int id){
        List<AnketaOdgovorProstor> prostori = aopServ.findByZgrada(zServ.findById(new Long(id)).get());
        List<AnketaOdgovorProstorDTO> prostoriDto = new ArrayList<>();
        for(AnketaOdgovorProstor aop: prostori){
            prostoriDto.add(new AnketaOdgovorProstorDTO(aop));
        }
        return prostoriDto;
    }

    @PostMapping("/post")
    public ResponseEntity<String> objavi(@RequestBody AnketaDTO dto){
        try{
            Anketa a = new Anketa(zServ.findById(new Long(dto.getIdZgrade())).get(), dto.getNazivAnkete(), dto.getTekstAnkete(), dto.getDtPocetak(), dto.getDtKraj());
            try{
                aServ.saveAnketa(a);
                return new ResponseEntity<>(a.getIdAnketa().toString(), HttpStatus.OK);
            }
            catch(Exception e){
                return new ResponseEntity<>("Anketa loša", HttpStatus.NOT_MODIFIED);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>("Došlo je do greške s anketom", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/postodg")
    public ResponseEntity<String> objaviodg(@RequestBody AnketaOdgovorDTO dto){
        try{
            AnketaOdgovor a = new AnketaOdgovor(aServ.findById(new Long(dto.getIdAnketa())).get(), dto.getOdgovor());
            try{
                aoServ.saveAnketaOdgovor(a);
                return new ResponseEntity<>(a.getAnketa().getIdAnketa().toString(), HttpStatus.OK);
            }
            catch(Exception e){
                return new ResponseEntity<>("Odgovori loši", HttpStatus.NOT_MODIFIED);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>("Došlo je do greške s odgovorima", HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/odg")
    public ResponseEntity<String> odgovori(@RequestBody AnketaOdgovorProstorDTO dto){
        try{
            List<AnketaOdgovorProstor> aops = aopServ.findByProstor(pServ.findById(new Long(dto.getIdProstor())).orElse(null));
            try {
                for (AnketaOdgovorProstor a : aops) {
                    if (a.getAnketa().getIdAnketa() == dto.getIdAnketa()) {
                        a.setOdgovor(dto.getOdgovor());
                        a.setOdgovorTekst(dto.getOdgovorTekst());
                        aopServ.saveAnketaOdgovor(a);
                        return new ResponseEntity<>(a.getAnketa().getIdAnketa().toString(), HttpStatus.OK);
                    }
                }
            }
            catch(Exception e){
                return new ResponseEntity<>("Neuspješno odgovoreno", HttpStatus.NOT_MODIFIED);
            }
            AnketaOdgovorProstor aop = new AnketaOdgovorProstor(aServ.findById(new Long(dto.getIdAnketa())).get(), pServ.findById(new Long(dto.getIdProstor())).get(),
                    dto.getOdgovor(), dto.getOdgovorTekst());
            try{
                aopServ.saveAnketaOdgovor(aop);
                return new ResponseEntity<>("Uspješno odgovoreno", HttpStatus.OK);
            }
            catch (Exception e){
                return new ResponseEntity<>("Neuspješno odgovoreno", HttpStatus.NOT_MODIFIED);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>("Neuspješno odgovoreno", HttpStatus.BAD_REQUEST);
        }
    }

}
