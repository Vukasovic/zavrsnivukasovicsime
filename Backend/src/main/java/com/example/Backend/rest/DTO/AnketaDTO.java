package com.example.Backend.rest.DTO;


import com.example.Backend.models.Anketa;

import java.sql.Date;

public class AnketaDTO {

    private int idAnketa;
    private int idZgrade;
    private String nazivAnkete;
    private String tekstAnkete;
    private Date dtPocetak;
    private Date dtKraj;
    private String rezultat;

    public AnketaDTO(
             int idAnketa,
             int idZgrade,
             String nazivAnkete,
             String tekstAnkete,
             Date dtPocetak,
             Date dtKraj,
             String rezultat){
        this.idAnketa = idAnketa;
        this.idZgrade = idZgrade;
        this.nazivAnkete = nazivAnkete;
        this.tekstAnkete = tekstAnkete;
        this.dtPocetak = dtPocetak;
        this.dtKraj = dtKraj;
        this.rezultat = rezultat;
    }

    public AnketaDTO(Anketa a){
        this.idAnketa = Math.toIntExact(a.getIdAnketa());
        this.idZgrade = Math.toIntExact(a.getZgrada().getIdZgrade());
        this.nazivAnkete = a.getNazivAnkete();
        this.tekstAnkete = a.getTekstAnkete();
        this.dtPocetak = a.getDtPocetak();
        this.dtKraj = a.getDtKraj();
        this.rezultat = a.getRezultat();
    }

    public int getIdAnketa() {
        return idAnketa;
    }

    public void setIdAnketa(int idAnketa) {
        this.idAnketa = idAnketa;
    }

    public int getIdZgrade() {
        return idZgrade;
    }

    public void setIdZgrade(int idZgrade) {
        this.idZgrade = idZgrade;
    }

    public String getNazivAnkete() {
        return nazivAnkete;
    }

    public void setNazivAnkete(String nazivAnkete) {
        this.nazivAnkete = nazivAnkete;
    }

    public String getTekstAnkete() {
        return tekstAnkete;
    }

    public void setTekstAnkete(String tekstAnkete) {
        this.tekstAnkete = tekstAnkete;
    }

    public Date getDtPocetak() {
        return dtPocetak;
    }

    public void setDtPocetak(Date dtPocetak) {
        this.dtPocetak = dtPocetak;
    }

    public Date getDtKraj() {
        return dtKraj;
    }

    public void setDtKraj(Date dtKraj) {
        this.dtKraj = dtKraj;
    }

    public String getRezultat() {
        return rezultat;
    }

    public void setRezultat(String rezultat) {
        this.rezultat = rezultat;
    }
}
