package com.example.Backend.rest;

import com.example.Backend.models.BrojClanova;
import com.example.Backend.models.Prostor;
import com.example.Backend.models.Zgrada;
import com.example.Backend.rest.DTO.AnketaOdgovorProstorDTO;
import com.example.Backend.rest.DTO.BrojClanovaDTO;
import com.example.Backend.rest.DTO.ProstorDTO;
import com.example.Backend.service.IBrojClanovaService;
import com.example.Backend.service.IProstorService;
import com.example.Backend.service.IVrstaProstoraService;
import com.example.Backend.service.IZgradaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/prostor")
public class ProstorController {

    private final String SEPARATOR = "$$$";
    private IProstorService pServ;
    private IZgradaService zServ;
    private IVrstaProstoraService vpServ;
    private IBrojClanovaService bcServ;

    @Autowired
    public ProstorController (IProstorService pServ, IZgradaService zServ, IVrstaProstoraService vpServ, IBrojClanovaService bcServ){

        this.pServ = pServ;
        this.zServ = zServ;
        this.vpServ = vpServ;
        this.bcServ = bcServ;
    }

    @GetMapping("/get/{id}")
    @ResponseBody
    public List<ProstorDTO> getProstori (@PathVariable int id){
        List<Prostor> prostori = pServ.findByZgrada(zServ.findById(new Long(id)).get());
        List<ProstorDTO> prostoriDto = new ArrayList<>();
        for (Prostor p: prostori){
            prostoriDto.add(new ProstorDTO(p));
        }
        return prostoriDto;
    }

    @GetMapping("/clanovi/get/{id}")
    @ResponseBody
    public List<BrojClanovaDTO> getClanovi(@PathVariable int id){
        List<BrojClanova> brojClanova = bcServ.findByProstor(pServ.findById(new Long(id)).get());
        List<BrojClanovaDTO> clanoviDto = new ArrayList<>();
        for (BrojClanova bc: brojClanova){
            clanoviDto.add(new BrojClanovaDTO((bc)));
        }
        return clanoviDto;
    }

    @GetMapping("/login/{email}/{psw}")
    public ResponseEntity<ProstorDTO> login (@PathVariable String email, @PathVariable String psw){
        try {
            Prostor prostor = pServ.findByVlasnikEmail(email).orElse(null);
            return new ResponseEntity<>(new ProstorDTO(prostor), HttpStatus.OK);
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/post")
    public ResponseEntity<String> promjena(@RequestBody ProstorDTO dto){
        try {
            Prostor p = pServ.findById(new Long(dto.getIdProstor())).orElse(null);
            p.setVlasnikEmail(dto.getVlasnikEmail());
            p.setVlasnikTel(dto.getVlasnikTel());
            try {
                pServ.saveProstor(p);
                return new ResponseEntity<>(p.getIdProstor().toString(), HttpStatus.OK);
            }
            catch(Exception e){
                return new ResponseEntity<>("Došlo je do greške", HttpStatus.NOT_MODIFIED);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>("Došlo je do greške", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/clanovi/post")
    public ResponseEntity<String> clanovi(@RequestBody BrojClanovaDTO dto){
        try{
            List<BrojClanova> brojevi = bcServ.findByProstor(pServ.findById(new Long(dto.getIdProstor())).orElse(null));
            try{
                for (BrojClanova broj: brojevi){
                    if (broj.getGodina() == dto.getGodina() && broj.getMjesec() == dto.getMjesec() && broj.getProstor().getIdProstor() == dto.getIdProstor()) {
                        broj.setBrojClanova(dto.getBrojClanova());
                        bcServ.saveBrojClanova(broj);
                        return new ResponseEntity<>(broj.getProstor().getIdProstor().toString() + SEPARATOR
                                + broj.getGodina() + SEPARATOR + broj.getMjesec(), HttpStatus.OK);
                    }
                }
            }catch(Exception e){
                return new ResponseEntity<>("Došlo je do greške", HttpStatus.NOT_MODIFIED);
            }
            BrojClanova b = new BrojClanova(pServ.findById(new Long(dto.getIdProstor())).get(), dto.getGodina(), dto.getMjesec(), dto.getBrojClanova(), dto.getPstOdUkupno(),dto.getNapomena());
            try{
                bcServ.saveBrojClanova(b);
                return new ResponseEntity<>("Promijenjeni podaci", HttpStatus.OK);
            }
            catch(Exception e){
                return new ResponseEntity<>("Došlo je do greške", HttpStatus.NOT_MODIFIED);
            }
        }catch(Exception e){
            return new ResponseEntity<>("Došlo je do greške", HttpStatus.BAD_REQUEST);
        }
    }
}
