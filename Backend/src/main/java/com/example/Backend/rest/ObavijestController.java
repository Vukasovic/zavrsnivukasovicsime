package com.example.Backend.rest;

import com.example.Backend.models.Obavijest;
import com.example.Backend.models.Zgrada;
import com.example.Backend.rest.DTO.ObavijestDTO;
import com.example.Backend.service.IObavijestService;
import com.example.Backend.service.IZgradaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/obavijest")
public class ObavijestController {

    private final String SEPARATOR = "$$$";
    private IObavijestService obServ;
    private IZgradaService zServ;

    @Autowired
    public ObavijestController(IObavijestService obServ, IZgradaService zServ){

        this.obServ = obServ;
        this.zServ = zServ;
    }

    @GetMapping("/get/{id}")
    @ResponseBody
    public List<ObavijestDTO> getObavijesti(@PathVariable int id){
        List<Obavijest> obavijesti = obServ.findByZgrada(zServ.findById(new Long(id)).get());
        List<ObavijestDTO> obavijestiDto = new ArrayList<>();
        for (Obavijest o: obavijesti){
            obavijestiDto.add(new ObavijestDTO(o));
        }
        return obavijestiDto;
    }

    @PostMapping("post")
    public ResponseEntity<String> post(@RequestBody ObavijestDTO dto){
        try{
            Obavijest o = new Obavijest(zServ.findById(new Long(dto.getIdZgrade())).get(), new Timestamp(dto.getDatumVrijeme().getTime()),
                    dto.getNaslovObavijesti(), dto.getObavijest());
            try{
                obServ.saveObavijest(o);
                return new ResponseEntity<>(o.getZgrada().getIdZgrade().toString() + SEPARATOR + o.getDatumVrijeme().getTime(), HttpStatus.OK);
            }
            catch(Exception e){
                return new ResponseEntity<>("Došlo je do greške", HttpStatus.NOT_MODIFIED);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>("Došlo je do greške", HttpStatus.BAD_REQUEST);
        }
    }
}
