package com.example.Backend.rest;

import com.example.Backend.models.VrstaProstora;
import com.example.Backend.models.Zgrada;
import com.example.Backend.rest.DTO.ZgradaDTO;
import com.example.Backend.service.IVrstaProstoraService;
import com.example.Backend.service.IZgradaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/zgrada")
public class ZgradaController {

    private IZgradaService zServ;
    private IVrstaProstoraService vpServ;

    @Autowired
    public ZgradaController(IZgradaService zServ,IVrstaProstoraService vpServ){
        this.zServ = zServ;
        this.vpServ = vpServ;
    }

    @GetMapping("/get")
    @ResponseBody
    public List<Zgrada> getZgrade(){
        return zServ.listAll();
    }

    @GetMapping("/find/{idZgrade}")
    @ResponseBody
    public ZgradaDTO getZgrada(@PathVariable int idZgrade) throws Exception{
        Zgrada z = zServ.findById(new Long(idZgrade)).orElse(null);
        return new ZgradaDTO(z);
    }

    @GetMapping("/vrste")
    @ResponseBody
    public List<VrstaProstora> vrste(){ return vpServ.listAll();}
}
