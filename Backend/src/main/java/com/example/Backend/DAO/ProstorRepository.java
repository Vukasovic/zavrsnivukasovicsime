package com.example.Backend.DAO;

import com.example.Backend.models.Prostor;
import com.example.Backend.models.Zgrada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProstorRepository extends JpaRepository<Prostor, Long> {

    List<Prostor> findByZgrada(Zgrada z);
    Optional<Prostor> findByVlasnikEmail(String email);
}
