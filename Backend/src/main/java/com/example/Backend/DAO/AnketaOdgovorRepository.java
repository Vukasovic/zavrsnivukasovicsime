package com.example.Backend.DAO;

import com.example.Backend.models.AnketaOdgovor;
import com.example.Backend.models.Zgrada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnketaOdgovorRepository extends JpaRepository<AnketaOdgovor, Long> {
}
