package com.example.Backend.DAO;

import com.example.Backend.models.StanjeBrojila;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StanjeBrojilaRepository extends JpaRepository<StanjeBrojila, Long> {
}
