package com.example.Backend.DAO;

import com.example.Backend.models.Parametri;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParametriRepository extends JpaRepository<Parametri, Long> {
}
