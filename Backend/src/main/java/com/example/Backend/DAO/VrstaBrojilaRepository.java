package com.example.Backend.DAO;

import com.example.Backend.models.VrstaBrojila;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VrstaBrojilaRepository extends JpaRepository<VrstaBrojila, String> {
}
