package com.example.Backend.DAO;

import com.example.Backend.models.Anketa;
import com.example.Backend.models.Zgrada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnketaRepository extends JpaRepository<Anketa, Long> {
    List<Anketa> findByZgrada(Zgrada zgrada);
}
