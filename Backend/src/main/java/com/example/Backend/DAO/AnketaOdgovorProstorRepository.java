package com.example.Backend.DAO;

import com.example.Backend.models.AnketaOdgovorProstor;
import com.example.Backend.models.Prostor;
import com.example.Backend.models.Zgrada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnketaOdgovorProstorRepository extends JpaRepository<AnketaOdgovorProstor, Long> {
    List<AnketaOdgovorProstor> findByProstor(Prostor prostor);
}
