package com.example.Backend.DAO;

import com.example.Backend.models.VrstaProstora;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VrstaProstoraRepository extends JpaRepository<VrstaProstora, String> {
}
