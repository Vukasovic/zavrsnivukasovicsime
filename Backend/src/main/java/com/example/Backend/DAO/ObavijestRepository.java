package com.example.Backend.DAO;

import com.example.Backend.models.Obavijest;
import com.example.Backend.models.Zgrada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObavijestRepository extends JpaRepository<Obavijest, Long> {
    List<Obavijest> findByZgrada(Zgrada zgrada);
}
