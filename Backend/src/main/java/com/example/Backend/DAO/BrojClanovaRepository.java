package com.example.Backend.DAO;

import com.example.Backend.models.BrojClanova;
import com.example.Backend.models.Prostor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrojClanovaRepository extends JpaRepository<BrojClanova, Long> {
    List<BrojClanova> findByProstor(Prostor p);
}
