package com.example.Backend.DAO;

import com.example.Backend.models.BrojiloZgrade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrojiloZgradeRepository extends JpaRepository<BrojiloZgrade, Long> {
}
