package com.example.Backend.DAO;

import com.example.Backend.models.Zgrada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZgradaRepository extends JpaRepository<Zgrada, Long> {
}
